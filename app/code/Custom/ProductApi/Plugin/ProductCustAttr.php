<?php
/** 
 */
namespace Custom\ProductApi\Plugin;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory;
use Divante\ReviewApi\Model\Converter\Review\ToDataModel;
use Magento\Store\Model\StoreManagerInterface;

class ProductCustAttr
{
      /**
     * @var CollectionFactory
     */
    private $reviewCollectionFactory;

     /**
     * @var ToDataModel
     */
    private $toDataModelConverter;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * GetList constructor.
     *
     * @param ToDataModel $toDataModelConvert
     * @param CollectionProcessorInterface $collectionProcessor
     * @param CollectionFactory $sourceCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param ReviewSearchResultInterfaceFactory $reviewSearchResultInterfaceFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct( 
        CollectionFactory $sourceCollectionFactory,
        ToDataModel $toDataModelConvert,
        StoreManagerInterface $storeManager
    ) { 
        $this->reviewCollectionFactory = $sourceCollectionFactory; 
        $this->toDataModelConverter = $toDataModelConvert;
        $this->storeManager = $storeManager;
    }
    public function afterGetList(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterface $searchCriteria
    ) : \Magento\Catalog\Api\Data\ProductSearchResultsInterface
    {
      
        $products = [];
        foreach ($searchCriteria->getItems() as $entity) {
            /** Get Current Extension Attributes from Product */
            
            $sku = $entity->getSku();
            $entityid = $entity->getEntityId();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($entityid);
            $bestis_in_stock=$productStockObj->getIsInStock();

            $collection = $this->reviewCollectionFactory->create();
            $collection->addStoreData();
            $collection->addFieldToFilter('sku', $sku);
            $collection->addRateVotes();

            $count=count($collection);
 
        $avgrating = ''; 
        if($count!='0'){

            /** @var \Magento\Catalog\Model\Product $productReview */
                

            foreach ($collection as $productReview) {
                $productReview->setCreatedAt($productReview->getReviewCreatedAt());
                $reviewDataObject = $this->toDataModelConverter->toDataModel($productReview);
 
                $rating=$reviewDataObject->getRatings(); 
                $total = 0;
                $res['value'] ='';
                $res['count'] ='';
                foreach ($rating as $prorate) {  
                    $total += $prorate->getValue();
                    $res['value']= $total;
                    $res['count']=count($rating);   
                }
                if (is_numeric($res['value']) && is_numeric($res['count'])) {

                    $avg =  $res['value']/$res['count']; 

                }
                
                $avgrating = round($avg);  
 
            } 

        } 


            $extensionAttributes = $entity->getExtensionAttributes();
            $extensionAttributes->setRating($avgrating); // custom field value set
            $extensionAttributes->setIsInStock($bestis_in_stock); // custom field value set
            $entity->setExtensionAttributes($extensionAttributes);
            $products[] = $entity;
        }
        $searchCriteria->setItems($products);
        return $searchCriteria;
    }


    /**
     * Convert Review Models to Data Models
     *
     * @param array $items
     *
     * @return array
     */
    private function convertItemsToDataModel(array $items): array
    {
        $data = [];

        foreach ($items as $item) {
            $dataModel = $this->toDataModelConverter->toDataModel($item);
            $dataModel->setStoreId($this->getStoreId());
            $data[] = $dataModel;
        }

        return $data;
    }

    /**
     * Retrive Store Id
     *
     * @return int
     */
    private function getStoreId(): int
    {
        return (int) $this->storeManager->getStore()->getId();
    }
    
}