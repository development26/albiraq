<?php

namespace Vendor\Checkoutcity\Model;

use Magento\Framework\Exception\CouldNotSaveException;

class PaymentInformationManagement extends \Magento\Checkout\Model\PaymentInformationManagement {

    public function savePaymentInformationAndPlaceOrder($cartId, \Magento\Quote\Api\Data\PaymentInterface $paymentMethod, \Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {


        $checkcity=$billingAddress->getCity();

        $acceptCites=array("Northwest Sulaibikhat","Doha","Kaerawan","Ali Subah Al-Salem","Wafra","Khiran City","Abdullah Port","West Abdullah Al-Mubarak","Shalehat Mina Abdullah","شمال غرب الصليبيخات","الدوحة","علي صباح السالم","الوفرة","الخيران السكنية","شاليهات ميناء عبدالله","القيروان","غرب عبدالله المبارك");

        //$total="9";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('quote'); //gives table name with prefix

        $sql = "Select * FROM " . $tableName." where entity_id =".$cartId;
        $result = $connection->fetchAll($sql);
        //echo $result->getGrandTotal();

        foreach ($result as $key) {
            $total=$key['subtotal'];
        }

        $tableadd = $resource->getTableName('quote_address'); //gives table name with prefix
        $sqls = "Select * FROM " . $tableadd." where quote_id =".$cartId." AND address_type='shipping'";
        $results = $connection->fetchAll($sqls);

        foreach ($results as $keys) {
            $getcheckcity=$keys['city'];
        }

        $this->messageManager->addWarning(__("10 Kwd ( Sorry, Minimum order amount for delivery for the selected address is 10 KWD )"));
        throw new LocalizedException(__("10 Kwd ( Sorry, Minimum order amount for delivery for the selected address is 10 KWD )"));
         exit;


        //var_dump($result);

        if(in_array($getcheckcity, $acceptCites)){
            if($total<="10"){
                throw new CouldNotSaveException(__('10 Kwd ( Sorry, Minimum order amount for delivery for the selected address is 10 KWD'));
                return false;
            }else{
                $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
                try {
                    $orderId = $this->cartManagement->placeOrder($cartId);
                } catch (\Exception $e) {
                    throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again.'), $e
                    );
                }
                return $orderId;

            }
        }else{
            $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
            try {
                $orderId = $this->cartManagement->placeOrder($cartId);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(
                __('An error occurred on the server. Please try to place the order again.'), $e
                );
            }
            return $orderId;
        }
    }
}