<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Custom\ProductApi\Model;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductLinkRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Backend\Media\EntryConverterPool;
use Magento\Catalog\Model\Product\Configuration\Item\Option\OptionInterface;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Pricing\SaleableInterface;


/**
 * Catalog product model
 *
 * @api
 * @method Product setHasError(bool $value)
 * @method null|bool getHasError()
 * @method array getAssociatedProductIds()
 * @method Product setNewVariationsAttributeSetId(int $value)
 * @method int getNewVariationsAttributeSetId()
 * @method int getPriceType()
 * @method string getUrlKey()
 * @method Product setUrlKey(string $urlKey)
 * @method Product setRequestPath(string $requestPath)
 * @method Product setWebsiteIds(array $ids)
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Product extends \Magento\Catalog\Model\Product
{   
     /**
     * Retrieve product category id
     *
     * @return int
     */
    public function getCategoryId()
    {
        if ($this->hasData('category_id')) {
            return $this->getData('category_id');
        }
        $category = $this->_registry->registry('current_category');
        $categoryId = $category ? $category->getId() : null;
        if ($categoryId && in_array($categoryId, $this->getCategoryIds())) {
            $this->setData('category_id', $categoryId);
            return $categoryId;
        }
        return false;
    }

   /**
     * Retrieve assigned category Ids
     *
     * @return array
     */
    public function getCategoryIds()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $area         = $objectManager->get('Magento\Framework\App\State');
        $areaCode     = $area->getAreaCode();

        if (!$this->hasData('category_ids')) {
            $wasLocked = false;
            if ($this->isLockedAttribute('category_ids')) {
                $wasLocked = true;
                $this->unlockAttribute('category_ids');
            }
            $ids = $this->_getResource()->getCategoryIds($this);           
            if($areaCode=='webapi_rest'){
              $this->setData('category_ids', "0");
            }else{
              $this->setData('category_ids', $ids);
            }
            if ($wasLocked) {
                $this->lockAttribute('category_ids');
            }
        }

        if($areaCode=='webapi_rest'){
        return (string) $this->_getData('category_ids');
        }else{                
        return (array) $this->_getData('category_ids');
        }


    }


    /**
     * Get product price through type instance
     *
     * @return string
     */
    public function getPrice($product = null)
    {
      $product = $product ?: $this;
      $proid=$product->getId();
        if ($this->_calculatePrice || !$this->getData(self::PRICE)) {
            return number_format($this->getPriceModel()->getPrice($this),3);
        } else {
            return number_format($this->getData(self::PRICE),3);
        }
    }
    

}
