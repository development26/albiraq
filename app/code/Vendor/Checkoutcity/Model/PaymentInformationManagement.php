<?php

namespace Vendor\Checkoutcity\Model;

use Magento\Framework\Exception\CouldNotSaveException;

class PaymentInformationManagement extends \Magento\Checkout\Model\PaymentInformationManagement {

    public function savePaymentInformationAndPlaceOrder($cartId, \Magento\Quote\Api\Data\PaymentInterface $paymentMethod, \Magento\Quote\Api\Data\AddressInterface $billingAddress = null) {


        $checkcity=$billingAddress->getCity();


        $ordertypedata=$paymentMethod->getPoNumber();
        if(!empty($ordertypedata)){
            $ordertype=$ordertypedata;
        }else{
            $ordertype="web";
        }

        //$total="9";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        if($ordertype!="web"){

                $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
                try {
                    $orderId = $this->cartManagement->placeOrder($cartId);
                } catch (\Exception $e) {
                    throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again.'), $e
                    );
                }
                $tableNames = $resource->getTableName('sales_order');
                $sqls = "UPDATE sales_order SET orderfrom = '".$ordertype."' WHERE entity_id =".$orderId;
                $connection->query($sqls);
                    

                $sqls = "Select * FROM sales_order where entity_id =".$orderId;
                $resultso = $connection->fetchAll($sqls);

                foreach ($resultso as $keys) {
                    $orderIds=$keys['increment_id'];
                }
                return $orderIds;

        }else{

                $this->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
                try {
                    $orderId = $this->cartManagement->placeOrder($cartId);
                } catch (\Exception $e) {
                    throw new CouldNotSaveException(
                    __('An error occurred on the server. Please try to place the order again.'), $e
                    );
                }
                $tableNames = $resource->getTableName('sales_order');
                $sqls = "UPDATE sales_order SET orderfrom = '".$ordertype."' WHERE entity_id =".$orderId;
                $connection->query($sqls);
                return $orderId;

        }
    }
}