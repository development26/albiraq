<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vendor\Checkoutcity\Model;

use Magento\Framework\Exception\CouldNotSaveException;

class GuestPaymentInformationManagement extends \Magento\Checkout\Model\GuestPaymentInformationManagement
{


    public function savePaymentInformationAndPlaceOrder(
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {

        $ordertypedata=$paymentMethod->getPoNumber();
        if(!empty($ordertypedata)){
            $ordertype=$ordertypedata;
        }else{
            $ordertype="web";
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

                $this->savePaymentInformation($cartId, $email, $paymentMethod, $billingAddress);
                try {
                    $orderId = $this->cartManagement->placeOrder($cartId);
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->getLogger()->critical(
                        'Placing an order with quote_id ' . $cartId . ' is failed: ' . $e->getMessage()
                    );
                    throw new CouldNotSaveException(
                        __($e->getMessage()),
                        $e
                    );
                } catch (\Exception $e) {
                    $this->getLogger()->critical($e);
                    throw new CouldNotSaveException(
                        __('An error occurred on the server. Please try to place the order again.'),
                        $e
                    );
                }

                    $tableNames = $resource->getTableName('sales_order');
                    $sqls = "UPDATE sales_order SET orderfrom = '".$ordertype."' WHERE entity_id =".$orderId;
                    $connection->query($sqls);

                return $orderId;


    }
}
