<?php

namespace Vendor\CustomApi\Model;

use Vendor\CustomApi\Api\TopRatedRepositoryInterface;

class TopRatedRepository extends ProductRepository implements TopRatedRepositoryInterface {

    /**
     * @inheritDoc
     */
    public function getList($limit = 20)
    {
        $collection = $this->getProductCollection();

        $now = date('Y-m-d H:i:s');
        $collection->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('image')
            ->addAttributeToSelect('small_image')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSelect('news_from_date')
            ->addAttributeToSelect('news_to_date')
            ->addAttributeToSelect('short_description')
            ->addAttributeToSelect('special_from_date')
            ->addAttributeToSelect('special_to_date')
            ->addAttributeToFilter('special_price', ['neq' => ''])
            ->addAttributeToFilter('special_from_date', ['lteq' => date('Y-m-d  H:i:s', strtotime($now))])
            ->addAttributeToFilter('special_to_date', ['gteq' => date('Y-m-d  H:i:s', strtotime($now))])
            ->addAttributeToFilter('is_saleable', ['eq' => 1], 'left');
        $collection->setPageSize(
            $limit
        )->setCurPage(
            1
        );

        //->getSelect()
        //     ->from(
        //         ['review_entity_summary' => $collection->getTable('review_entity_summary')],
        //         ['summary' => 'SUM(review_entity_summary.rating_summary)']

        //     )->where('e.entity_id = review_entity_summary.entity_pk_value'
        //     )->group(
        //         'review_entity_summary.entity_pk_value'
        //     )->order(
        //         'summary ' . \Magento\Framework\DB\Select::SQL_DESC
        //     )->having(
        //         'SUM(review_entity_summary.rating_summary) > ?',
        //         0
        //     );

        $collection->load();
        //$collection->addCategoryIds();
        // TODO: add cache here for products as in ProductRepository

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->count());

        return $searchResults;
    }
}