<?php

namespace Vendor\CustomApi\Model;

use Vendor\CustomApi\Api\BestSellerRepositoryInterface;

class BestSellerRepository extends ProductRepository implements BestSellerRepositoryInterface {

    /**
     * @inheritDoc
     */
    public function getList($limit = 10)
    {
        $collection = $this->getProductCollection();
        $connection = $collection->getConnection();
        $orderTableAliasName = $connection->quoteIdentifier('order');
        $orderJoinCondition = [
            $orderTableAliasName . '.entity_id = order_items.order_id',
            $connection->quoteInto("{$orderTableAliasName}.state <> ?", \Magento\Sales\Model\Order::STATE_CANCELED),
        ];

        $collection->setPageSize(
            $limit
        )->setCurPage(
            1
        )->getSelect()
            ->from(
                ['order_items' => $collection->getTable('sales_order_item')],
                ['ordered_qty' => 'SUM(order_items.qty_ordered)','product_id']
            )->joinInner(
                ['order' => $collection->getTable('sales_order')],
                implode(' AND ', $orderJoinCondition),
                []
            )->where(
                'e.entity_id = order_items.product_id and parent_item_id IS NULL'
            )->group(
                'order_items.product_id'
            )->order(
                'ordered_qty ' . \Magento\Framework\DB\Select::SQL_DESC
            )->having(
                'SUM(order_items.qty_ordered) > ?',
                0
            );

        $collection->load();
        //$collection->addCategoryIds();

        foreach ($collection as $item) {
            $restypeid=$item['type_id'];

            if($restypeid=="simple"){

            $productInfo = $item['entity_id'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $products = $objectManager->create('Magento\Catalog\Model\Product')->load($productInfo);
            $_attributeValue = $products->getCustomAttributes();

            //print_r($_attributeValue);
            $res['id']=(int)$item['entity_id'];
            $res['sku']=$item['sku'];
            $res['name']=$item['name'];
            $res['attribute_set_id']=(int)$item['attribute_set_id'];
            $res['price']=number_format($item['price'],3);
            $res['status']=(int)$item['status'];
            $res['visibility']=(int)$item['visibility'];
            $res['type_id']=$item['type_id'];
            $res['created_at']=$item['created_at'];
            $res['updated_at']=$item['updated_at'];

            $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($item['entity_id']);
            $res['is_in_stock']=$productStockObj->getIsInStock();

            $res['custom_attributes']=array();
            foreach ($_attributeValue as $keys => $value) {
                $resp['attribute_code']=$value->getAttributeCode();
                $resp['value']=$value->getValue();
                $res['custom_attributes'][]=$resp;
            }

            $respo['items'][]=$res;
        }

        }

        // TODO: add cache here for products as in ProductRepository
        //$collection->getItems();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setItems($respo['items']);
        $searchResults->setTotalCount($collection->count());

        return $searchResults;
    }
}