<?php

namespace Vendor\CustomApi\Model;

use Magento\Framework\App\ProductMetadataInterface;

class Version implements \Vendor\CustomApi\Api\VersionInterface {
    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(ProductMetadataInterface $productMetadata)
    {
        $this->productMetadata = $productMetadata;
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function getVersion() {
        $version = $this->productMetadata->getVersion();
        return $version;
    }
}