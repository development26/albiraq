<?php

namespace Vendor\CustomApi\Api\PayPal\Transparent;

interface SecurityTokenManagerInterface {

    /**
     * Set category name
     *
     * @param int $quoteId
     * @return Vendor\CustomApi\Api\PayPal\Transparent\SecurityTokenInterface
     */
    public function getSecurityToken($quoteId);
}
