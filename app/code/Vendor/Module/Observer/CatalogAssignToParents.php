<?php

namespace Vendor\Module\Observer;

class CatalogAssignToParents implements \Magento\Framework\Event\ObserverInterface
{
    /** @var \Magento\Catalog\Api\CategoryLinkManagementInterface */
    protected $categoryLinkManagement;

    /**
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     */
    function __construct(
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
    ) {
        $this->categoryLinkManagement = $categoryLinkManagement;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();

        $categories = [];
        /** @var \Magento\Catalog\Model\Category $category */
        foreach($product->getCategoryCollection() as $category) {
            foreach($category->getParentCategories() as $parentCategory) {
                $categories[] = $parentCategory->getId();
            }
        }
        $categories = array_unique($categories);

        $this->categoryLinkManagement->assignProductToCategories($product->getSku(), $categories);
    }
}