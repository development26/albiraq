<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Currentproduct extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {


        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $collection = $productCollection->addAttributeToSelect('*')
                    ->load();

        foreach ($collection as $product){
             echo $sku = $product->getSku()."<br>";
             $categories = $product->getCategoryIds();
             //print_r($categories);

                $categories = [];
                /** @var \Magento\Catalog\Model\Category $category */
                foreach($product->getCategoryCollection() as $category) {
                    foreach($category->getParentCategories() as $parentCategory) {
                        $categories[] = $parentCategory->getId();
                        //print_r($categories);
                    }
                }
                print_r($categories);
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $categoryLinkRepository = $objectManager->get('\Magento\Catalog\Api\CategoryLinkManagementInterface');
                $categoryLinkRepository->assignProductToCategories($sku, $categories);

            exit;
        } 



        // $sku = 'Tyn-TB-A13';
        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $_product = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface')->get($sku,true, 0, true);
        // $_product->setCustomAttribute("country_of_manufacture", 'KW');
        // $_product->save($_product); 


        //Insert Data into table
        date_default_timezone_set('Asia/Kuwait');
        $date=date('Y-m-d');
        $time=date('H:i:s');

        if(!empty($date)){

            $jsons['date']=$date;
            $jsons['time']=$time;
            $jsons['msg']="Data found";
            $jsons['error']=1;

        }else{

            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";


        }


        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}