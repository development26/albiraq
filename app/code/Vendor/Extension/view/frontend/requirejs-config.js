var config = {
config: {
    mixins: {
        'Magento_Checkout/js/view/summary/abstract-total': {
            'Vendor_Extension/js/view/summary/abstract-total-mixin': true
        },
        'Magento_Checkout/js/view/summary/shipping': {
            'Vendor_Extension/js/view/summary/shipping-mixin': true
        },
    }
}};
