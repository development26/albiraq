define(
[
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/cart/totals-processor/default'
],
function (stepNavigator, quote, totalsDefaultProvider) {
    "use strict";

    var estimateTotalsShipping = function () {
        totalsDefaultProvider.estimateTotals(quote.shippingAddress());
    };

    // this should be added to get correct totals
    quote.shippingMethod.subscribe(estimateTotalsShipping);

    return function (abstractTotal) {
        return abstractTotal.extend({
            isFullMode: function() {
                if (!this.getTotals() || stepNavigator.getActiveItemIndex() === 1) {
                    return false;
                }
                return true; //add this line to display forcefully summary in shipping step.
            }
        });
    }
});