<?php

namespace Meetanshi\Cbk\Controller\Adminhtml\Payment;

use Magento\Sales\Model\OrderFactory;
use Meetanshi\Cbk\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Framework\Data\Form\FormKey\Validator;

/**
 * Class Request
 * @package Meetanshi\Cbk\Controller\Adminhtml\Payment
 */
class Request extends Action
{
    /**
     * @var OrderFactory
     */
    protected $orderFactory;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var Validator
     */
    protected $formKeyValidator;


    /**
     * Request constructor.
     * @param Action\Context $context
     * @param OrderFactory $orderFactory
     * @param Validator $formKeyValidator
     * @param Data $helper
     * @param array $params
     */
    public function __construct(
        Action\Context $context,
        OrderFactory $orderFactory,
        Validator $formKeyValidator,
        Data $helper,
        $params = []
    )
    {
        $this->orderFactory = $orderFactory;
        $this->helper = $helper;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $this->messageManager->addErrorMessage(__('Unable to process Inquiry Service.'));
            return $this->resultRedirectFactory->create()->setPath(
                'sales/order/view',
                [
                    'order_id' => $params['order_id']
                ]
            );
        }
        if ($params) {
            $resultRedirect = $this->resultRedirectFactory->create();

            $order = $this->orderFactory->create()->load($params['order_id']);
            $payments = $order->getPayment()->getAdditionalInformation();
            $clientId = $this->helper->getClientID();
            $clientSecret = $this->helper->getClientSecret();
            $encryptedKey = $this->helper->getEncryptKey();
            $gatewayUrl = $this->helper->getGatewayUrl();

            if (array_key_exists('pay_id', $payments)) {
                if ($payments['pay_id'] == '') {
                    $this->messageManager->addErrorMessage(__('Payment Id is not available for this order..'));
                    return $this->resultRedirectFactory->create()->setPath(
                        'sales/order/view',
                        [
                            'order_id' => $order->getId()
                        ]
                    );
                }
            } else {
                $this->messageManager->addErrorMessage(__('Payment Id is not available for this order..'));
                return $this->resultRedirectFactory->create()->setPath(
                    'sales/order/view',
                    [
                        'order_id' => $order->getId()
                    ]
                );
            }

            $postfield = [
                "ClientId" => $clientId,
                "ClientSecret" => $clientSecret,
                "ENCRP_KEY" => $encryptedKey
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $gatewayUrl . "/ePay/api/cbk/online/pg/merchant/Authenticate",
                CURLOPT_ENCODING => "",
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_POSTFIELDS => json_encode($postfield),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic ' . base64_encode($clientId . ":" . $clientSecret),
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            if ($err) {
                $this->messageManager->addErrorMessage(__('Authorization is Failed.'));
                return $this->resultRedirectFactory->create()->setPath(
                    'sales/order/view',
                    [
                        'order_id' => $order->getId()
                    ]
                );
            }
            curl_close($curl);

            $authenticateData = json_decode($response);
            if ($authenticateData->Status == "1") {
                $accessToken = $authenticateData->AccessToken;
                $payId = $payments['pay_id'];

                $postfield = [
                    "encrypmerch" => $encryptedKey,
                    "authkey" => $accessToken,
                    "payid" => $payId
                ];

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $gatewayUrl . "/ePay/api/cbk/online/pg/Verify",
                    CURLOPT_ENCODING => "",
                    CURLOPT_FOLLOWLOCATION => 1,
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_FRESH_CONNECT => true,
                    CURLOPT_POSTFIELDS => json_encode($postfield),
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Basic ' . base64_encode($clientId . ":" . $clientSecret),
                        "Content-Type: application/json",
                        "cache-control: no-cache"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                if ($err) {
                    $this->messageManager->addErrorMessage(__('Verification is Failed.'));
                    return $this->resultRedirectFactory->create()->setPath(
                        'sales/order/view',
                        [
                            'order_id' => $order->getId()
                        ]
                    );
                }

                $paymentDetails = json_decode($response);

                if ($paymentDetails->Status != "0" && $paymentDetails->Status != "-1" && $paymentDetails->Status != "2" && $paymentDetails->Status != "3") {
                    $payment = $order->getPayment();
                    $paymentID = $paymentDetails->PaymentId;
                    $trackId = $paymentDetails->TrackId;
                    $presult = $paymentDetails->Message;
                    $tranId = $paymentDetails->TransactionId;
                    $ref = $paymentDetails->ReferenceId;
                    $authcode = $paymentDetails->AuthCode;
                    $payId = $paymentDetails->PayId;

                    if ($paymentID) {
                        $payment->setAdditionalInformation('payment_id', $paymentID);
                    }
                    if ($trackId) {
                        $payment->setAdditionalInformation('track_id', $trackId);
                    }
                    if ($presult) {
                        $payment->setAdditionalInformation('result', $presult);
                    }
                    if ($tranId) {
                        $payment->setAdditionalInformation('tran_id', $tranId);
                    }
                    if ($ref) {
                        $payment->setAdditionalInformation('reference_id', $ref);
                    }
                    if ($authcode) {
                        $payment->setAdditionalInformation('auth_code', $authcode);
                    }
                    if ($payId) {
                        $payment->setAdditionalInformation('pay_id', $payId);
                    }
                    $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
                    $payment->save();

                    $order->save();
                } else {
                    $payment = $order->getPayment();
                    $paymentID = $paymentDetails->PaymentId;
                    $trackId = $paymentDetails->TrackId;
                    $presult = $paymentDetails->Message;
                    $tranId = $paymentDetails->TransactionId;
                    $ref = $paymentDetails->ReferenceId;
                    $authcode = $paymentDetails->AuthCode;
                    $payId = $paymentDetails->PayId;

                    if ($paymentID) {
                        $payment->setAdditionalInformation('payment_id', $paymentID);
                    }
                    if ($trackId) {
                        $payment->setAdditionalInformation('track_id', $trackId);
                    }
                    if ($presult) {
                        $payment->setAdditionalInformation('result', $presult);
                    }
                    if ($tranId) {
                        $payment->setAdditionalInformation('tran_id', $tranId);
                    }
                    if ($ref) {
                        $payment->setAdditionalInformation('reference_id', $ref);
                    }
                    if ($authcode) {
                        $payment->setAdditionalInformation('auth_code', $authcode);
                    }
                    if ($payId) {
                        $payment->setAdditionalInformation('pay_id', $payId);
                    }
                    $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
                    $payment->save();

                    $order->save();
                }
            }

            return $this->resultRedirectFactory->create()->setPath(
                'sales/order/view',
                [
                    'order_id' => $order->getId()
                ]
            );
        }
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
