<?php

namespace Meetanshi\Cbk\Controller\Payment;

use Meetanshi\Cbk\Controller\Main;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment\Transaction;

/**
 * Class Success
 * @package Meetanshi\Cbk\Controller\Payment
 */
class Success extends Main implements CsrfAwareActionInterface
{
    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = $this->request->getParams();

        if (array_key_exists('encrp', $params)) {
            $clientId = $this->helper->getClientID();
            $clientSecret = $this->helper->getClientSecret();
            $encryptedKey = $this->helper->getEncryptKey();
            $gatewayUrl = $this->helper->getGatewayUrl();

            $postfield = [
                "ClientId" => $clientId,
                "ClientSecret" => $clientSecret,
                "ENCRP_KEY" => $encryptedKey
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $gatewayUrl . "/ePay/api/cbk/online/pg/merchant/Authenticate",
                CURLOPT_ENCODING => "",
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_POSTFIELDS => json_encode($postfield),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic ' . base64_encode($clientId . ":" . $clientSecret),
                    "Content-Type: application/json",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            if ($err) {
                $this->messageManager->addErrorMessage(__('Result Authorization is Failed.'));
                $this->checkoutSession->restoreQuote();
                $this->_redirect('checkout/cart');
            }
            curl_close($curl);

            $authenticateData = json_decode($response);
            if ($authenticateData->Status == "1") {
                $accessToken = $authenticateData->AccessToken;

                $url = $gatewayUrl . "/ePay/api/cbk/online/pg/GetTransactions/" . $params['encrp'] . "/" . $accessToken;
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_ENCODING => "",
                    CURLOPT_FOLLOWLOCATION => 1,
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Basic ' . base64_encode($clientId . ":" . $clientSecret),
                        "Content-Type: application/json",
                        "cache-control: no-cache"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                $paymentDetails = json_decode($response);
                $order = $this->orderFactory->create()->loadByIncrementId($paymentDetails->MerchUdf1);
                if ($paymentDetails->Status != "0" && $paymentDetails->Status != "-1" && $paymentDetails->Status != "2" && $paymentDetails->Status != "3") {
                    $payment = $order->getPayment();
                    $paymentID = $paymentDetails->PaymentId;
                    $trackId = $paymentDetails->TrackId;
                    $presult = $paymentDetails->CCMessage;
                    $tranId = $paymentDetails->TransactionId;
                    $ref = $paymentDetails->ReferenceId;
                    $payType = $paymentDetails->PayType;
                    $authcode = $paymentDetails->AuthCode;
                    $payId = $paymentDetails->PayId;
                    $postDate = $paymentDetails->PostDate;
                    $amount = $paymentDetails->Amount;

                    $payment->setTransactionId($tranId);
                    $payment->setLastTransId($tranId);
                    if ($paymentID) {
                        $payment->setAdditionalInformation('payment_id', $paymentID);
                    }
                    if ($payType) {
                        $payment->setAdditionalInformation('payment_type', $payType);
                    }
                    if ($trackId) {
                        $payment->setAdditionalInformation('track_id', $trackId);
                    }
                    if ($presult) {
                        $payment->setAdditionalInformation('result', $presult);
                    }
                    if ($tranId) {
                        $payment->setAdditionalInformation('tran_id', $tranId);
                    }
                    if ($ref) {
                        $payment->setAdditionalInformation('reference_id', $ref);
                    }
                    if ($authcode) {
                        $payment->setAdditionalInformation('auth_code', $authcode);
                    }
                    if ($payId) {
                        $payment->setAdditionalInformation('pay_id', $payId);
                    }
                    if ($postDate) {
                        $payment->setAdditionalInformation('post_date', $postDate);
                    }
                    if ($amount) {
                        $payment->setAdditionalInformation('amount', $amount);
                    }

                    $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
                    $trans = $this->transactionBuilder;
                    $transaction = $trans->setPayment($payment)->setOrder($order)->setTransactionId($tranId)->setAdditionalInformation((array)$payment->getAdditionalInformation())->setFailSafe(true)->build(Transaction::TYPE_CAPTURE);

                    $payment->setParentTransactionId(null);

                    $payment->save();

                    $this->orderSender->notify($order);
                    $order->setState(Order::STATE_PROCESSING, true);
                    $order->setStatus(Order::STATE_PROCESSING);

                    $order->addStatusHistoryComment(__('Transaction is approved by the bank'), Order::STATE_PROCESSING)->setIsCustomerNotified(true);

                    $order->save();

                    $transaction->save();
                    if ($this->helper->isAutoInvoice()) {
                        if ($order->canInvoice()) {
                            $invoice = $this->invoiceService->prepareInvoice($order);
                            if (!$invoice) {
                                $order->addStatusHistoryComment('Can\'t generate the invoice right now.', false);
                            }

                            if (!$invoice->getTotalQty()) {
                                $order->addStatusHistoryComment('Can\'t generate an invoice without products.', false);
                            }
                            $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
                            $invoice->register();
                            $invoice->getOrder()->setCustomerNoteNotify(true);
                            $invoice->getOrder()->setIsInProcess(true);
                            $transactionSave = $this->transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
                            $transactionSave->save();

                            try {
                                $this->invoiceSender->send($invoice);
                            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                                $order->addStatusHistoryComment('Can\'t send the invoice Email right now.', false);
                            }

                            $order->addStatusHistoryComment('Automatically Invoice Generated.', false);
                            $order->save();
                        }
                    }
                    $this->messageManager->addSuccessMessage(__('Transaction is approved by the bank'));
                    $this->_redirect('checkout/onepage/success');
                    return;
                } else {
                    $payment = $order->getPayment();
                    $paymentID = $paymentDetails->PaymentId;
                    $trackId = $paymentDetails->TrackId;
                    $presult = $paymentDetails->CCMessage;
                    $payType = $paymentDetails->PayType;
                    $tranId = $paymentDetails->TransactionId;
                    $ref = $paymentDetails->ReferenceId;
                    $authcode = $paymentDetails->AuthCode;
                    $payId = $paymentDetails->PayId;
                    $postDate = $paymentDetails->PostDate;
                    $amount = $paymentDetails->Amount;

                    $payment->setTransactionId($tranId);
                    $payment->setLastTransId($tranId);
                    if ($paymentID) {
                        $payment->setAdditionalInformation('payment_id', $paymentID);
                    }
                    if ($payType) {
                        $payment->setAdditionalInformation('payment_type', $payType);
                    }
                    if ($trackId) {
                        $payment->setAdditionalInformation('track_id', $trackId);
                    }
                    if ($presult) {
                        $payment->setAdditionalInformation('result', $presult);
                    }
                    if ($tranId) {
                        $payment->setAdditionalInformation('tran_id', $tranId);
                    }
                    if ($ref) {
                        $payment->setAdditionalInformation('reference_id', $ref);
                    }
                    if ($authcode) {
                        $payment->setAdditionalInformation('auth_code', $authcode);
                    }
                    if ($payId) {
                        $payment->setAdditionalInformation('pay_id', $payId);
                    }
                    if ($postDate) {
                        $payment->setAdditionalInformation('post_date', $postDate);
                    }
                    if ($amount) {
                        $payment->setAdditionalInformation('amount', $amount);
                    }

                    $payment->setAdditionalInformation((array)$payment->getAdditionalInformation());
                    $payment->save();
                    $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank'));
                    $msg = 'Your transaction is not approved by bank.';
                    $order->addStatusHistoryComment($msg, Order::STATE_CANCELED)->setIsCustomerNotified(true);
                    $order->cancel();
                    $order->save();
                    $this->checkoutSession->restoreQuote();
                    $this->_redirect('checkout/cart');
                    return;
                }
            } else {
                $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank'));
                $this->checkoutSession->restoreQuote();
                $this->_redirect('checkout/cart');
                return;
            }

        } else {
            $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank'));
            $this->checkoutSession->restoreQuote();
            $this->_redirect('checkout/cart');
            return;
        }

        $this->messageManager->addErrorMessage(__('Transaction is not approved by the bank'));
        $this->checkoutSession->restoreQuote();
        $this->_redirect('checkout/cart');
        return;
    }
}
