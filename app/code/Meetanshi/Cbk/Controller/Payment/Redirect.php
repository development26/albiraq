<?php

namespace Meetanshi\Cbk\Controller\Payment;

use Meetanshi\Cbk\Controller\Main;
use Magento\Sales\Model\Order;

/**
 * Class Redirect
 * @package Meetanshi\Cbk\Controller\Payment
 */
class Redirect extends Main
{
    /**
     * @return false|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $result = $this->jsonFactory->create();
            try {
                $order = $this->checkoutSession->getLastRealOrder();
                $order->setState(Order::STATE_PENDING_PAYMENT, true);
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
                $order->save();
                $html = $this->helper->getPaymentForm($order);
                return $result->setData(['error' => false, 'success' => true, 'html' => $html]);
            } catch (\Exception $e) {
                $this->checkoutSession->restoreQuote();
                return $result->setData(['error' => true, 'success' => false, 'message' => __('Authenication Failed.')]);
            }
        }
        return false;
    }
}
