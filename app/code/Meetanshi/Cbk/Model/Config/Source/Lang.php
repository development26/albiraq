<?php

namespace Meetanshi\Cbk\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Lang
 * @package Meetanshi\Cbk\Model\Config\Source
 */
class Lang implements Arrayinterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [['value' => 'en', 'label' => __('English')], ['value' => 'ar', 'label' => __('Arabic')],];
    }
}
