<?php

namespace Meetanshi\Cbk\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\Cbk\Helper\Data;


/**
 * Class CbkConfigProvider
 * @package Meetanshi\Cbk\Model
 */
class CbkConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $methodCodes = ['cbk'];
    /**
     * @var array
     */
    protected $methods = [];


    /**
     * CbkConfigProvider constructor.
     * @param Data $helper
     * @param PaymentHelper $paymentHelper
     * @param StoreManagerInterface $storeManager
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(Data $helper, PaymentHelper $paymentHelper, StoreManagerInterface $storeManager)
    {
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        foreach ($this->methodCodes as $code) {
            $this->methods[$code] = $paymentHelper->getMethodInstance($code);
        }
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        $redirectUrl = $this->storeManager->getStore()->getBaseUrl() . 'cbk/payment/redirect';
        $showLogo = $this->helper->showLogo();
        $imageUrl = $this->helper->getPaymentLogo();

        $config = [];
        $config['payment']['cbk_payment']['imageurl'] = ($showLogo) ? $imageUrl : '';
        $config['payment']['cbk_payment']['payment_instruction'] = trim($this->helper->getPaymentInstructions());
        $config['payment']['cbk_payment']['redirect_url'] = $redirectUrl;

        return $config;
    }
}
