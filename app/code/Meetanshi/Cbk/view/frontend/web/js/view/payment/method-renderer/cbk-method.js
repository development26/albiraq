/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/full-screen-loader',
        'mage/url',
    ],
    function (
        $,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        additionalValidators,
        fullScreenLoader,
        url) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Meetanshi_Cbk/payment/cbk',
                redirectAfterPlaceOrder: false
            },
            getCode: function () {
                return 'cbk';
            },
            isActive: function () {
                return true;
            },
            context: function () {
                return this;
            },
            getCbkInstructions: function () {
                return window.checkoutConfig.payment.cbk_payment.payment_instruction;
            },

            getCbkLogoUrl: function () {
                return window.checkoutConfig.payment.cbk_payment.imageurl;
            },

            afterPlaceOrder: function () {
                var self = this;
                this.isPlaceOrderActionAllowed(false);
                fullScreenLoader.startLoader();
                var html;

                $.when(sendRequest()).done(function () {
                    self.isPlaceOrderActionAllowed(true);
                    $("body").append(html);
                    $("#CbkHostedForm").submit();
                }).fail(function () {
                    self.isPlaceOrderActionAllowed(true);
                });

                function sendRequest() {
                    return $.ajax({
                        type: 'POST',
                        url: window.checkoutConfig.payment.cbk_payment.redirect_url,
                        dataType: "json",
                        success: function (response) {
                            console.log(response);
                            if (response.success) {
                                html = response.html;
                                fullScreenLoader.stopLoader();
                            } else {
                                self.messageContainer.addErrorMessage({
                                    message: response.message || "Fail, please try again later."
                                });
                                fullScreenLoader.stopLoader();
                            }
                        },
                        error: function (response) {
                            fullScreenLoader.stopLoader();
                            self.messageContainer.addErrorMessage({
                                message: "Error, please try again later"
                            });
                        }
                    });
                }
            }
        });
    }
);
