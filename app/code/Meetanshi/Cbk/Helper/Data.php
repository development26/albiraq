<?php

namespace Meetanshi\Cbk\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Asset\Repository;

/**
 * Class Data
 * @package Meetanshi\Cbk\Helper
 */
class Data extends AbstractHelper
{
    const CONFIG_CBK_ACTIVE = 'payment/cbk/active';

    const CONFIG_CBK_MODE = 'payment/cbk/mode';

    const CONFIG_CBK_LANGUAGE = 'payment/cbk/lang';

    const CONFIG_CBK_ALIAS = 'payment/cbk/alias';

    const CONFIG_CBK_INSTRUCTIONS = 'payment/cbk/instructions';

    const CONFIG_CBK_INVOICE = 'payment/cbk/allow_invoice';

    const CONFIG_CBK_LOGO = 'payment/cbk/show_logo';
    const CONFIG_CBK_PAYMENT_TYPE = 'payment/cbk/payment_type';

    const CONFIG_SANDBOX_CLIENT_ID = 'payment/cbk/sandbox_client_id';
    const CONFIG_LIVE_CLIENT_ID = 'payment/cbk/live_client_id';
    const CONFIG_SANDBOX_CLIENT_SECRET = 'payment/cbk/sandbox_secret_key';
    const CONFIG_LIVE_CLIENT_SECRET = 'payment/cbk/live_secret_key';
    const CONFIG_SANDBOX_ENCRYPTION_KEY = 'payment/cbk/sandbox_encrypted_key';
    const CONFIG_LIVE_ENCRYPTION_KEY = 'payment/cbk/live_encrypted_key';
    const CONFIG_SANDBOX_GATEWAY_URL = 'payment/cbk/sandbox_gateway_url';
    const CONFIG_LIVE_GATEWAY_URL = 'payment/cbk/live_gateway_url';
    /**
     * @var Http
     */
    protected $request;
    /**
     * @var DirectoryList
     */
    protected $directoryList;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var Repository
     */
    private $repository;
    /**
     * @var RequestInterface
     */
    private $requestInterface;
    /**
     * @var EncryptorInterface
     */
    protected $encryptor;


    /**
     * Data constructor.
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param DirectoryList $directoryList
     * @param StoreManagerInterface $storeManager
     * @param Repository $repository
     * @param RequestInterface $requestInterface
     * @param Http $request
     */
    public function __construct(Context $context, EncryptorInterface $encryptor, DirectoryList $directoryList, StoreManagerInterface $storeManager, Repository $repository, RequestInterface $requestInterface, Http $request)
    {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->directoryList = $directoryList;
        $this->storeManager = $storeManager;
        $this->requestInterface = $requestInterface;
        $this->request = $request;
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function getGatewayUrl()
    {
        if ($this->getMode()) {
            return $this->scopeConfig->getValue(self::CONFIG_SANDBOX_GATEWAY_URL, ScopeInterface::SCOPE_STORE);
        } else {
            return $this->scopeConfig->getValue(self::CONFIG_LIVE_GATEWAY_URL, ScopeInterface::SCOPE_STORE);
        }
    }

    /**
     * @return string
     */
    public function getClientID()
    {
        if ($this->getMode()) {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_SANDBOX_CLIENT_ID, ScopeInterface::SCOPE_STORE));
        } else {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_LIVE_CLIENT_ID, ScopeInterface::SCOPE_STORE));
        }
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        if ($this->getMode()) {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_SANDBOX_CLIENT_SECRET, ScopeInterface::SCOPE_STORE));
        } else {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_LIVE_CLIENT_SECRET, ScopeInterface::SCOPE_STORE));
        }
    }

    /**
     * @return string
     */
    public function getEncryptKey()
    {
        if ($this->getMode()) {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_SANDBOX_ENCRYPTION_KEY, ScopeInterface::SCOPE_STORE));
        } else {
            return $this->encryptor->decrypt($this->scopeConfig->getValue(self::CONFIG_LIVE_ENCRYPTION_KEY, ScopeInterface::SCOPE_STORE));
        }
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_MODE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_PAYMENT_TYPE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isAutoInvoice()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_INVOICE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getPaymentLanguage()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_LANGUAGE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function showLogo()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_LOGO, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getPaymentLogo()
    {
        $params = ['_secure' => $this->request->isSecure()];
        return $this->repository->getUrlWithParams('Meetanshi_Cbk::images/cbk.png', $params);
    }

    /**
     * @return mixed
     */
    public function getPaymentInstructions()
    {
        return $this->scopeConfig->getValue(self::CONFIG_CBK_INSTRUCTIONS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $storeId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSuccessUrl($storeId)
    {
        $baseUrl = $this->storeManager->getStore($storeId)->getBaseUrl();
        return $baseUrl . "cbk/payment/success";
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreName()
    {
        if ($this->storeManager->getStore()->getName()) {
            return $this->storeManager->getStore()->getName();
        } else {
            return 'Magento 2 Order';
        }
    }

    /**
     * @param $order
     * @return string
     */
    public function getPaymentForm($order)
    {
        $amount = number_format($order->getGrandTotal(), 3);
        $currencyCode = 'KWD';
        $storeId = $order->getStoreId();

        $clientId = $this->getClientID();
        $clientSecret = $this->getClientSecret();
        $encryptedKey = $this->getEncryptKey();
        $gatewayUrl = $this->getGatewayUrl();

        $postfield = [
            "ClientId" => $clientId,
            "ClientSecret" => $clientSecret,
            "ENCRP_KEY" => $encryptedKey
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $gatewayUrl . "/ePay/api/cbk/online/pg/merchant/Authenticate",
            CURLOPT_ENCODING => "",
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FRESH_CONNECT => true,
            CURLOPT_POSTFIELDS => json_encode($postfield),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic ' . base64_encode($clientId . ":" . $clientSecret),
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            throw new \Exception('Authentication Failed.');
        }

        curl_close($curl);

        $authenticateData = json_decode($response);
        if ($authenticateData->Status == "1") {
            //save access token till expiry
            $accessToken = $authenticateData->AccessToken;

            $formData = array(
                'tij_MerchantEncryptCode' => $encryptedKey,
                'tij_MerchAuthKeyApi' => $accessToken,
                'tij_MerchantPaymentLang' => $this->getPaymentLanguage(),
                'tij_MerchantPaymentAmount' => $amount,
                'tij_MerchantPaymentTrack' => $order->getIncrementId(),
                'tij_MerchantPaymentRef' => $this->getStoreName(),
                'tij_MerchantPaymentCurrency' => $currencyCode,
                'tij_MerchantUdf1' => $order->getIncrementId(),
                'tij_MerchantUdf2' => '',
                'tij_MerchantUdf3' => '',
                'tij_MerchantUdf4' => '',
                'tij_MerchantUdf5' => '',
                //'tij_MerchPayType' => $this->getPaymentType(),
                'tij_MerchReturnUrl' => $this->getSuccessUrl($storeId)
            );

            $url = $gatewayUrl . "/ePay/pg/epay?_v=" . $accessToken;
            $form = "<form id='CbkHostedForm' method='post' action='$url' enctype='application/x-www-form-urlencoded'>";
            foreach ($formData as $k => $v) {
                $form .= "<input type='hidden' name='$k' value='$v'>";
            }
            $form .= "</form>";
            return $form;
        } else {
            throw new \Exception('Authentication Failed.');
        }
    }
}
