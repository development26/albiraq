<?php
use \Magento\Framework\Component\ComponentRegistrar;

\Magento\Framework\Component\ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Meetanshi_Cbk',
    __DIR__
);
