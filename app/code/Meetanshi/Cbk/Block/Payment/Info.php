<?php

namespace Meetanshi\Cbk\Block\Payment;

use Magento\Payment\Block\ConfigurableInfo;

/**
 * Class Info
 * @package Meetanshi\Cbk\Block\Payment
 */
class Info extends ConfigurableInfo
{
    /**
     * @var string
     */
    protected $_template = 'Meetanshi_Cbk::payment/info.phtml';

    /**
     * @param string $field
     * @return \Magento\Framework\Phrase|string
     */
    public function getLabel($field)
    {
        switch ($field) {
            case 'method_title':
                return __('Method Title');
            case 'payment_id':
                return __('Payment ID');
            case 'payment_type':
                return __('Payment Type');
            case 'track_id':
                return __('Track ID');
            case 'result':
                return __('Transaction Status');
            case 'tran_id':
                return __('Transaction ID');
            case 'reference_id':
                return __('Reference ID');
            case 'auth_code':
                return __('Auth Code');
            case 'pay_id':
                return __('Pay ID');
            case 'post_date':
                return __('Post Date');
            case 'amount':
                return __('Amount');
            default:
                return parent::getLabel($field);
        }
    }
}
