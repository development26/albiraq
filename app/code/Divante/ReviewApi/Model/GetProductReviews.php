<?php
/**
 * Copyright Divante Sp. z o.o.
 * See LICENSE_DIVANTE.txt for license details.
 */
declare(strict_types=1);

namespace Divante\ReviewApi\Model;

use Divante\ReviewApi\Api\GetProductReviewsInterface;
use Magento\Review\Model\ResourceModel\Review\Product\Collection as ReviewCollection;
use Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory as ReviewCollectionFactory;
use Divante\ReviewApi\Model\Converter\Review\ToDataModel as ReviewConverter;

/**
 * Class GetProductReviews load product reviews by product sku
 */
class GetProductReviews implements GetProductReviewsInterface
{
    /**
     * @var ReviewConverter
     */
    private $reviewConverter;

    /**
     * @var ReviewCollectionFactory
     */
    private $reviewCollectionFactory;

    /**
     * GetProductReviews constructor.
     *
     * @param ReviewConverter $reviewConverter
     * @param ReviewCollectionFactory $collectionFactory
     */
    public function __construct(
        ReviewConverter $reviewConverter,
        ReviewCollectionFactory $collectionFactory
    ) {
        $this->reviewConverter = $reviewConverter;
        $this->reviewCollectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     *
     * @param string $sku
     *
     * @return array|\Divante\ReviewApi\Api\Data\ReviewInterface[]
     */
    public function execute(string $sku)
    {
        /** @var ReviewCollection $collection */
        $collection = $this->reviewCollectionFactory->create();

       /* echo "<pre>"; print_r($collection->getData());

        exit;*/
        $collection->addStoreData();
        $collection->addFieldToFilter('sku', $sku);
        $collection->addRateVotes();

        //$reviews = [];


        $count=count($collection);

        if($count!='0'){

            /** @var \Magento\Catalog\Model\Product $productReview */
            
            foreach ($collection as $productReview) {
                $productReview->setCreatedAt($productReview->getReviewCreatedAt());
                $reviewDataObject = $this->reviewConverter->toDataModel($productReview);
                //$reviews[] = $reviewDataObject;
                $status_id=$reviewDataObject->getReviewStatus();
                if($status_id=="1"){

                    $json['id']=$reviewDataObject->getId();
                    $json['title']=$reviewDataObject->getTitle();
                    $json['detail']=$reviewDataObject->getDetail();
                    $json['nickname']=$reviewDataObject->getNickname();
                    $rating=$reviewDataObject->getRatings();
                    $json['ratings']= array();
                    foreach ($rating as $prorate) {
                        $res['vote_id']=$prorate->getVoteId();
                        $res['rating_id']=$prorate->getRatingId();
                        $res['rating_name']=$prorate->getRatingName();
                        $res['percent']=$prorate->getPercent();
                        $res['value']=$prorate->getValue();
                        $json['ratings'][]=$res;
                    }
                    
                    $json['review_entity']=$reviewDataObject->getReviewEntity();
                    $json['review_type']=$reviewDataObject->getReviewType();
                    $json['review_status']=$reviewDataObject->getReviewStatus();
                    $json['created_at']=$reviewDataObject->getCreatedAt();
                    $json['entity_pk_value']=$reviewDataObject->getEntityPkValue();
                    $json['store_id']=$reviewDataObject->getStoreId();

                    $response[] =$json;

                    $jsons['list']=$response;
                    $jsons['msg']="Data found";
                    $jsons['error']=1;

                }else{

                    $jsons['list']="";
                    $jsons['error']=0;
                    $jsons['msg']="Data not found";

                }

            }

        }else{

            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";


        }

        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;
        //return $reviews;
    }
}
