<?php
namespace Magecomp\Codverification\Controller\Sendotp;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Otp extends \Magento\Framework\App\Action\Action
{
    protected $helperapi;
    protected $helperdata;
    protected $resultJson;
    protected $checkoutSession;
    protected $emailfilter;

    public function __construct(Context $context,
                                \Magecomp\Codverification\Helper\Apicall $helperapi,
                                \Magecomp\Codverification\Helper\Data $helperdata,
                                JsonFactory $resultJson,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Email\Model\Template\Filter $filter)
    {
        $this->helperapi = $helperapi;
        $this->helperdata = $helperdata;
        $this->resultJson = $resultJson;
        $this->checkoutSession = $checkoutSession;
        $this->emailfilter = $filter;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        
        if(isset($data['type'])=="device"){

            try
            {
                $resultJson = $this->resultJson->create();

                if(!$this->helperdata->isEnabled())
                {
                    $response = [
                        'error' => true,
                        'msg' => __("Cash On Delivery Verification is Disabled.")
                    ];
                    return $resultJson->setData($response);
                }

                $mobilenumbers = $data['mobile'];//$this->checkoutSession->getQuote()->getBillingAddress()->getTelephone();
                $mobilenumber = str_replace("+", "", $mobilenumbers);

                if($mobilenumber == '' || $mobilenumber == null)
                {
                    $response = [
                        'error' => 0,
                        'msg' => __("Please, Add Your Billing Address Telephone Number First.")
                    ];
                    return $resultJson->setData($response);
                }


                $otp = $this->helperdata->getOtp();
                $this->emailfilter->setVariables(['otp' => $otp]);
                $message = $this->helperdata->getOtpTemplate();
                $finalmessage = $this->emailfilter->filter($message);



                $responce = $this->helperapi->callApiUrl($mobilenumber,$finalmessage);
                
                if($responce === true)
                {
                    // $quote = $this->checkoutSession->getQuote();
                    // $quote->setOtp($otp);
                    // $quote->save();

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $connection = $resource->getConnection();
                    $tableName = $resource->getTableName('quote'); //gives table name with 
                    //Update Data into table
                    $sql = "Update " . $tableName . " Set otp =".$otp." where entity_id =".$data['quote'];
                    $connection->query($sql);

                    $response = [
                        'error' => 1,
                        'msg' => __("OTP Send Successfully.")
                    ];
                    return $resultJson->setData($response);
                }

                $response = [
                    'errors' => true,
                    'message' => $responce
                ];
                return $resultJson->setData($response);

            } catch(\Magento\Framework\Exception\LocalizedException $e) {
                $response = [
                    'errors' => true,
                    'message' => $e->getMessage()
                ];
                return $resultJson->setData($response);
            }

        }else{

            try
            {
                $resultJson = $this->resultJson->create();

                if(!$this->helperdata->isEnabled())
                {
                    $response = [
                        'errors' => true,
                        'message' => __("Cash On Delivery Verification is Disabled.")
                    ];
                    return $resultJson->setData($response);
                }

                $mobilenumbers = $this->checkoutSession->getQuote()->getBillingAddress()->getTelephone();
                $mobilenumber = str_replace("+", "", $mobilenumbers);

                if($mobilenumber == '' || $mobilenumber == null)
                {
                    $response = [
                        'errors' => true,
                        'message' => __("Please, Add Your Billing Address Telephone Number First.")
                    ];
                    return $resultJson->setData($response);
                }


                $otp = $this->helperdata->getOtp();
                $this->emailfilter->setVariables(['otp' => $otp]);
                $message = $this->helperdata->getOtpTemplate();
                $finalmessage = $this->emailfilter->filter($message);



                $responce = $this->helperapi->callApiUrl($mobilenumber,$finalmessage);
                
                if($responce === true)
                {
                    $quote = $this->checkoutSession->getQuote();
                    $quote->setOtp($otp);
                    $quote->save();

                    $response = [
                        'errors' => false,
                        'message' => __("OTP Send Successfully.")
                    ];
                    return $resultJson->setData($response);
                }

                $response = [
                    'errors' => true,
                    'message' => $responce
                ];
                return $resultJson->setData($response);

            } catch(\Magento\Framework\Exception\LocalizedException $e) {
                $response = [
                    'errors' => true,
                    'message' => $e->getMessage()
                ];
                return $resultJson->setData($response);
            }

        }
    }
}
