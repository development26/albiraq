<?php
namespace Magecomp\Codverification\Controller\Verify;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Json\Helper\Data as Jsonhelper;

class Otp extends \Magento\Framework\App\Action\Action
{
    protected $helperdata;
    protected $resultJson;
    protected $checkoutSession;
    protected $jsonhelper;

    public function __construct(Context $context,
                                \Magecomp\Codverification\Helper\Data $helperdata,
                                JsonFactory $resultJson,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                Jsonhelper $jsonhelper)
    {

        $this->helperdata = $helperdata;
        $this->resultJson = $resultJson;
        $this->checkoutSession = $checkoutSession;
        $this->jsonhelper = $jsonhelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();

        if(isset($data['type'])=="device"){
            try
            {
                $resultJson = $this->resultJson->create();

                //$postdata = $this->jsonhelper->jsonDecode($this->getRequest()->getContent());
                $otp = $data['otp'];//$postdata['codcode'];
                $getquoteid=$data['quote'];

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                $tableName = $resource->getTableName('quote'); //gives table name with prefix

                //Select Data from table
                $sql = "Select * FROM " . $tableName." Where otp=".$otp." AND entity_id=".$getquoteid;
                $resultss = $connection->fetchAll($sql);

                $count=count($resultss);
               
                foreach ($resultss as $key) {
                    $quoteotp=$key['otp'];
                }


                if($count=="0")
                {
                    $response = [
                        'error' => 0,
                        'msg' => __("Invalid OTP.")
                    ];
                    return $resultJson->setData($response);
                }
                else
                {
                    $tableNames = $resource->getTableName('quote'); //gives table name with 
                    //Update Data into table
                    $sqls = "Update " . $tableNames . " Set codverification = 1 where entity_id =".$data['quote'];
                    $connection->query($sqls);

                    // $quote = $this->checkoutSession->getQuote();
                    // $quote->setCodverification(1);
                    // $quote->save();

                    $response = [
                        'error' => 1,
                        'msg' => __("Cash On Delivery Verification is Completed.")
                    ];
                    return $resultJson->setData($response);
                }
            } catch(\Magento\Framework\Exception\LocalizedException $e) {
                $response = [
                    'errors' => true,
                    'message' => $e->getMessage()
                ];
                return $resultJson->setData($response);
            }

        }else{

            try
            {
                $resultJson = $this->resultJson->create();

                if(!$this->helperdata->isEnabled())
                {
                    $response = [
                        'errors' => true,
                        'message' => __("Cash On Delivery Verification is Disabled.")
                    ];
                    return $resultJson->setData($response);
                }

                $postdata = $this->jsonhelper->jsonDecode($this->getRequest()->getContent());
                $otp = $postdata['codcode'];
                $quoteotp = $this->checkoutSession->getQuote()->getOtp();

                if($otp != $quoteotp)
                {
                    $response = [
                        'errors' => true,
                        'message' => __("Invalid OTP.")
                    ];
                    return $resultJson->setData($response);
                }
                else
                {
                    $quote = $this->checkoutSession->getQuote();
                    $quote->setCodverification(1);
                    $quote->save();

                    $response = [
                        'errors' => false,
                        'message' => __("Cash On Delivery Verification is Completed.")
                    ];
                    return $resultJson->setData($response);
                }
            } catch(\Magento\Framework\Exception\LocalizedException $e) {
                $response = [
                    'errors' => true,
                    'message' => $e->getMessage()
                ];
                return $resultJson->setData($response);
            }
        }
    }
}
