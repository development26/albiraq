<?php 
namespace Magecomp\Coddanubesms\Helper;

class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_DANUBESMS_API_CAMP = 'codverification/smsgatways/danubesmscamp';
    const XML_DANUBESMS_API_FROM = 'codverification/smsgatways/danubesmsfrom';
	const XML_DANUBESMS_API_USERNAME = 'codverification/smsgatways/danubesmsusername';
	const XML_DANUBESMS_API_PASSWORD = 'codverification/smsgatways/danubesmspassword';
	const XML_DANUBESMS_API_URL = 'codverification/smsgatways/danubesmsapiurl';


	public function __construct(\Magento\Framework\App\Helper\Context $context)
	{
		parent::__construct($context);
	}

    public function getTitle() {
        return __("Danubesms");
    }

    public function getApiCamp(){
        return $this->scopeConfig->getValue(
            self::XML_DANUBESMS_API_CAMP,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiFrom(){
        return $this->scopeConfig->getValue(
            self::XML_DANUBESMS_API_FROM,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiUsername(){
        return $this->scopeConfig->getValue(
            self::XML_DANUBESMS_API_USERNAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiPassword(){
        return $this->scopeConfig->getValue(
            self::XML_DANUBESMS_API_PASSWORD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

	public function getApiUrl()	{
		return $this->scopeConfig->getValue(
            self::XML_DANUBESMS_API_URL,
			 \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
	}

    public function validateSmsConfig()
    {
        return $this->getApiCamp() && $this->getApiFrom() && $this->getApiUsername() && $this->getApiPassword() && $this->getApiUrl();
    }

    public function callApiUrl($mobilenumbers,$message)
    {
        try
        {   

            $from = $this->getApiFrom();
            $camp = $this->getApiCamp();
            $url = $this->getApiUrl();
            $user = $this->getApiUsername();
            $password = $this->getApiPassword();


            if(strpos($mobilenumbers, '971') === false) {
                $newTelephone=preg_replace('/^0?/', '971', $mobilenumbers);
            } else if(strpos($mobilenumbers, '+971') === false) {
                $newTelephone=preg_replace('/^0?/', '971', $mobilenumbers);
            } else {
                $newTelephone= $mobilenumbers;
            }

            $ch = curl_init();
            if (!$ch){
                return "Couldn't initialize a cURL handle";
            }

            $smsurl = $url."?from=".$from."&to=".$newTelephone."&msg=".urlencode($message)."&user=".$user."&password=".$password."&camp=".$camp."&dr=1";
        
            curl_setopt($ch, CURLOPT_URL, $smsurl);
            curl_setopt($ch,CURLOPT_HEADER,0);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            
            $curlresponse = curl_exec($ch); // execute
            $curl_info = curl_getinfo($ch);


            if ($curlresponse == FALSE)
            {
                return "cURL error: ".curl_error($ch);
            }
            elseif($curl_info['http_code'] != 200)
            {
                return "Error: non-200 HTTP status code: ".$curl_info['http_code'];
            }
            else
            {
                $result= json_decode($curlresponse,true);

                $status_code = $result['status'];
                if($status_code == 'success')
                {
                    return true;
                }
                return "Error sending: status code [".$status_code."] description [".$curlresponse."]";
            }
            curl_close($ch);
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}