<?php
namespace Magecomp\Cityandregionmanager\Api\Data;

interface GroupCitiesInterface
{
    const ID            = 'region_id';
    const STATES_NAME   = 'default_name';
    const CITIES_NAME   = 'default_name';

    public function getId();

    public function getStatesName();

    public function getCitiesName();

    public function setId($id);

    public function setStatesName($states_name);

    public function setCitiesName($cities_name);

}