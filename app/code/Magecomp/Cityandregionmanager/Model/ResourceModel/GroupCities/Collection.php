<?php
namespace Magecomp\Cityandregionmanager\Model\ResourceModel\GroupCities;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magecomp\Cityandregionmanager\Api\Data\GroupCitiesInterface;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'region_id';

    protected $_eventPrefix = 'magecomp_cityandregionmanager_groupcities_collection';

    protected $_eventObject = 'groupcities_collection';

    protected function _construct()
    {
        $this->_init('Magecomp\Cityandregionmanager\Model\Cities', 'Magecomp\Cityandregionmanager\Model\ResourceModel\GroupCities');
    }

    protected function _toOptionArray($valueField = null, $labelField = 'title', $additional = [])
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
}