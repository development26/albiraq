<?php

namespace Magecomp\Cityandregionmanager\Ui\Component\Listing\Column;

use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use Magento\Directory\Model\CountryFactory;

class Country extends Column
{
    protected $_searchCriteria;
    protected $_countryFactory;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CountryFactory $countryFactory,
        array $components = [],
        array $data = [])
    {
        $this->_countryFactory = $countryFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $countries = $this->_countryFactory->create();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if($item['country_id'])
                    $countryname = $countries->loadByCode($item['country_id'])->getName();
                else
                    $countryname ="";
                $item[$this->getData('name')] = $countryname;
            }
        }

        return $dataSource;
    }
}