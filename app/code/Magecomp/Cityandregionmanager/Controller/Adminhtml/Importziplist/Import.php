<?php
namespace Magecomp\Cityandregionmanager\Controller\Adminhtml\Importziplist;

use Magento\Backend\App\Action;
use Magento\Framework\File\Csv;
use Magecomp\Cityandregionmanager\Model\Zip;
use Magecomp\Cityandregionmanager\Model\ResourceModel\Zip\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Import extends Action
{
    protected $_csv;

    protected $_zipListModel;

    protected $_zipListCollection;

    protected $_directoryList;

    public function __construct(
        Action\Context $context,
        Csv $csv,
        Zip $zipListModel,
        DirectoryList $directoryList,
        CollectionFactory $zipListCollectionFactory
    ) {
        $this->_csv                  = $csv;
        $this->_zipListModel      = $zipListModel;
        $this->_zipListCollection = $zipListCollectionFactory;
        $this->_directoryList        = $directoryList;
        parent::__construct($context);
    }

    public function execute()
    {
        ini_set('max_execution_time', '150');
        $resultRedirect = $this->resultRedirectFactory->create();
        $zip_list = $this->_zipListModel;

        $zip_list_collection = $this->_zipListCollection->create();
        $data = $zip_list_collection->getData();
        $zip_list_city = [];

        foreach ($data as $item)
        {
            $zip_list_city[] = $item['zip_code'];
        }

        $tmpDir = $this->_directoryList->getPath('tmp');
        $file = $tmpDir . '/datasheet-zipList.csv';

        if (!isset($file)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please upload valid CSV file.'));
        }

        $csv = $this->_csv;
        $csv->setDelimiter(',');
        $csvData = $csv->getData($file);
        $duplicate = 0;
        $imported = 0;
        foreach ($csvData as $row => $data) {
            if (count($data) == 3)
            {
                if ($data[0] == 'State name') continue;
                if($data[0] == "" || $data[1] == "" || $data[2] == "") continue;

                if (!in_array($data[2],$zip_list_city))
                {
                    $zip_list->setData([
                        'states_name' => $data[0],
                        'cities_name' => $data[1],
                        'zip_code'    => $data[2]
                    ])->save();
                    $imported++;
                }
                else{
                    $duplicate++;
                }
            }
            else{
                $this->messageManager->addError('The list of states should be in three column!');
                return $resultRedirect->setPath('*/*/index');
            }
        }
        if($imported)
            $this->messageManager->addSuccess($imported.' records imported successfully!');
        if($duplicate)
            $this->messageManager->addWarning($duplicate.' records not import because already exist.');
        return $resultRedirect->setPath('magecomp_cityandregionmanager/zip/index');
    }
}