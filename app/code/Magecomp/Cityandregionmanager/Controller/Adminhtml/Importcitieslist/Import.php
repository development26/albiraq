<?php
namespace Magecomp\Cityandregionmanager\Controller\Adminhtml\Importcitieslist;

use Magento\Backend\App\Action;
use Magento\Framework\File\Csv;
use Magecomp\Cityandregionmanager\Model\Cities;
use Magecomp\Cityandregionmanager\Model\ResourceModel\Cities\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;

class Import extends Action
{
    protected $_csv;

    protected $_citiesListModel;

    protected $_citiesListCollection;

    protected $_directoryList;

    public function __construct(
        Action\Context $context,
        Csv $csv,
        Cities $citiesListModel,
        DirectoryList $directoryList,
        CollectionFactory $citiesListCollectionFactory
    ) {
        $this->_csv                  = $csv;
        $this->_citiesListModel      = $citiesListModel;
        $this->_citiesListCollection = $citiesListCollectionFactory;
        $this->_directoryList        = $directoryList;
        parent::__construct($context);
    }

    public function execute()
    {
        ini_set('max_execution_time', '100');
        $resultRedirect = $this->resultRedirectFactory->create();
        $cities_list = $this->_citiesListModel;

        $cities_list_collection = $this->_citiesListCollection->create();
        $data = $cities_list_collection->getData();
        $cities_list_city = [];

        foreach ($data as $item)
        {
            $cities_list_city[] = $item['cities_name'];
        }

        $tmpDir = $this->_directoryList->getPath('tmp');
        $file = $tmpDir . '/datasheet-citiesList.csv';

        if (!isset($file)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please upload valid CSV file.'));
        }

        $csv = $this->_csv;
        $csv->setDelimiter(',');
        $csvData = $csv->getData($file);
        $duplicate = 0;
        $imported = 0;
        foreach ($csvData as $row => $data) {
            if (count($data) == 2)
            {
                if ($data[0] == 'State name') continue;
                if($data[0] == "" || $data[1] == "") continue;

                if (!in_array($data[1],$cities_list_city))
                {
                    $cities_list->setData([
                        'states_name' => $data[0],
                        'cities_name' => $data[1]
                    ])->save();
                    $imported++;
                }
                else{
                    $duplicate++;
                }
            }
            else{
                $this->messageManager->addError('The list of states should be in two column!');
                return $resultRedirect->setPath('*/*/index');
            }
        }
        if($imported)
            $this->messageManager->addSuccess($imported.' records imported successfully!');
        if($duplicate)
            $this->messageManager->addWarning($duplicate.' records not import because already exist.');

        return $resultRedirect->setPath('magecomp_cityandregionmanager/cities/index');
    }
}