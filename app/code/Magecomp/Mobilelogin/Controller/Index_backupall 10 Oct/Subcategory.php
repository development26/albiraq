<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Subcategory extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $data = "false";
        $parentCatId = $this->getRequest()->get('parentCatId');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // instance of object manager

        $categoryRepository = $objectManager->create('Magento\Catalog\Model\CategoryRepository');
        $parentcategories = $categoryRepository->get($parentCatId);
        $categories = $parentcategories->getChildrenCategories();

        $count=count($categories);

        if($count!='0'){

          foreach ($categories as $key => $value) {
              $res['entity_id']=$value->getEntityId();
              $cateid=$value->getEntityId();
              $res['name']=$value->getName();

              $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
              $cateinstance = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
              $allcategoryproduct = $cateinstance->create()->load($cateid)->getProductCollection()->addAttributeToSelect('*');
              $res['count'] = $allcategoryproduct->count();

              $response[] =$res;
          }
            $jsons['list']=$response;
            $jsons['msg']="Category list found";
            $jsons['error']=1;
        }else{
            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";
        }


        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}