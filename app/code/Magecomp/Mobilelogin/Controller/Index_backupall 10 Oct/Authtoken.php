<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Authtoken extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $usr = [
        'username' => 'pavnish',
        'password' => 'Pavnish@#123'
    ];

    $baseUrl=$this->_storeManager->getStore()->getBaseUrl();

    $ch = curl_init($baseUrl.'rest/V1/integration/admin/token');

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($usr));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: '. strlen(json_encode($usr))]);

    $token = curl_exec($ch);
 
    $token1=substr_replace($token ,"", -1);

    $str1 = substr($token1, 1);

    $numlength = strlen((string)$str1);


    if($numlength=="32"){
        $modifiedresult['token']=$str1;
        $modifiedresult['error']="1";
        $modifiedresult['success']="true";
        $modifiedresult['msg']="Admin token has been successfully generated";
    }else{
        $modifiedresult['token']="";
        $modifiedresult['error']="0";
        $modifiedresult['success']="false";
        $modifiedresult['msg']="Please check credentials";
    }

    echo json_encode($modifiedresult);

    // echo '======================== <br />';

    // $ch = curl_init('http://15.184.29.132/rest/V1/orders/items');

    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Bearer '. json_decode($token)]);

    // $result = curl_exec($ch);

    // echo '<h1>Results</h1><hr />';
    // echo '<pre>'. print_r($result) .'</pre>';

        /********* get feature product **************/

        //print_r($collection->getData());

        // $returnVal="exist";

        // if($returnVal=="exist"){
        //     $data = ['list'=>$json, 'msg' => 'Data found.', 'error' => '1'];
        // }else{
        //     $data = ['msg' => 'No data found.', 'error' => '0'];
        // }

        // $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        // $resultJson->setData($data);
        // return $resultJson;

    }
}