<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Ajaxforgototpverifyapp extends \Magento\Framework\App\Action\Action
{
    public $_helperdata;
    public function __construct(
        Context $context,
        \Magecomp\Mobilelogin\Helper\Data $helperData)
    {
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute() {
        $data = $this->getRequest()->getParams();
        $returnVal = $this->_helperdata->verfiyForgotOtp($data['mobile'],$data['otp']);

        
      if($returnVal=="true"){
        $datas = ['msg' => 'Otp verify successfully.', 'error' => 1];
      }else{
        $datas = ['msg' => 'Please enter valid otp.', 'error' => 0];
      }


      $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
      $resultJson->setData($datas);
      return $resultJson;

        //$this->getResponse()->setBody($returnVal);
    }
}