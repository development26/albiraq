<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\RegotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;

class Ajaxcheckregotpformobile extends \Magento\Framework\App\Action\Action
{
    protected $_modelRegOtpFactory;

    public function __construct(
        Context $context,
        RegotpmodelFactory $modelRegOtpFactory
    )
    {
        $this->_modelRegOtpFactory = $modelRegOtpFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if($this->getRequest()->get('flag') == 1){
          $mobile = $this->getRequest()->get('mobile');
          $otp = $this->getRequest()->get('otp');
          $smrId = $this->getRequest()->get('userId');
          $otpModels = $this->_modelRegOtpFactory->create();
          $collection = $otpModels->getCollection();
          $collection->addFieldToFilter('mobile', $mobile);
          $collection->addFieldToFilter('random_code', $otp);

          $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
          $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
          $connection = $resource->getConnection();
          $tableName = $resource->getTableName('sms_register_otp'); //gives table name with prefix



          if (count($collection) == '1') {
              $sql = "UPDATE " . $tableName . " SET is_verify = 1 where mobile =".$mobile;
              $connection->query($sql);
              $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
              $customer = $customerFactory->load($smrId);
              $billingAddressId = $customer->getDefaultBilling();
              $shippingAddressId = $customer->getDefaultShipping();
              $addressRepository = $objectManager->create('Magento\Customer\Api\AddressRepositoryInterface');
              $address = $addressRepository->getById($billingAddressId);
              $address->setTelephone($mobile);
              $addressRepository->save($address);
              $address = $addressRepository->getById($shippingAddressId);
              $address->setTelephone($mobile);
              $addressRepository->save($address);
              $data = "1";
              $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
              $resultJson->setData($data);
              return $resultJson;
          } else {
              $data = "false";
              $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
              $resultJson->setData($data);
              return $resultJson;
          }
        }
        else{
          $mobile = $this->getRequest()->get('mobile');
          $otp = $this->getRequest()->get('otp');

          $otpModels = $this->_modelRegOtpFactory->create();
          $collection = $otpModels->getCollection();
          $collection->addFieldToFilter('mobile', $mobile);
          $collection->addFieldToFilter('random_code', $otp);

          $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
          $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
          $connection = $resource->getConnection();
          $tableName = $resource->getTableName('sms_register_otp'); //gives table name with prefix



          if (count($collection) == '1') {
              $sql = "UPDATE " . $tableName . " SET is_verify = 1 where mobile =".$mobile;
              $connection->query($sql);
              //$data = "true";
              $data = ['msg' => 'Otp verify successfully', 'error' => 1];
              $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
              $resultJson->setData($data);
              return $resultJson;
          } else {
              //$data = "false";
              $data = ['msg' => 'Wrong Otp, Please enter vaild Otp', 'error' => 0];
              $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
              $resultJson->setData($data);
              return $resultJson;
          }
        }
    }
}
