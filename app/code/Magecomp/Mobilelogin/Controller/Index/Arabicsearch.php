<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Arabicsearch extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $quoteid = $this->getRequest()->get('q');

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $geturl=$this->_storeManager->getStore()->getBaseUrl();

            //arabuic Data from table
            $sql = "SELECT * FROM catalog_product_entity_varchar WHERE store_id='2' AND value LIKE '%".$quoteid."%'";
            $productCollection = $connection->fetchAll($sql);

            //sku search data in table
            $sqls = "SELECT * FROM catalog_product_entity WHERE sku LIKE '%".$quoteid."%'";
            $productski = $connection->fetchAll($sqls);


            $count= count($productCollection);

            $skucount= count($productski);

            $json['code']="product";

            if($count!="0"){
                foreach ($productCollection as $key) {
                    //$values=$key['value'];
                    $entityid=$key['entity_id'];

                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($entityid);

                    
                    $res['sku']=$product->getSku();
                    $res['image']=$geturl.'pub/media/catalog/product'.$product->getImage();
                    $res['description']=$product->getDescription();
                    $res['price']=number_format($product->getPrice(),3);
                    $res['url']="";
                    $res['special_price']=number_format($product->getSpecialPrice(),3);
                    $res['entity_id']=(int)$product->getEntityId();
                    $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getEntityId());
                    $res['is_in_stock']=$productStockObj->getIsInStock();
                    $res['special_form_date']=$product->getSpecialFromDate();
                    $res['special_to_date']=$product->getSpecialToDate();
                    $arabicproduct = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(1)->load($product->getEntityId());
                    $res['arname']=$arabicproduct->getName();
                    $res['name']=$product->getName();
                    $json['items'][]=$res;
                    $json['count']= $count;
                    $json['url']= $geturl."catalogsearch/result/?q=".$quoteid;
                }

            }elseif($skucount!="0"){
                
                foreach ($productski as $key) {
                    //$values=$key['value'];
                    $entityid=$key['entity_id'];

                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($entityid);

                    $res['sku']=$product->getSku();
                    $res['image']=$geturl.'pub/media/catalog/product'.$product->getImage();
                    $res['description']=$product->getDescription();
                    $res['price']=number_format($product->getPrice(),3);
                    $res['url']="";
                    $res['special_price']=number_format($product->getSpecialPrice(),3);
                    $res['entity_id']=(int)$product->getEntityId();
                    $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getEntityId());
                    $res['is_in_stock']=$productStockObj->getIsInStock();
                    $res['special_form_date']=$product->getSpecialFromDate();
                    $res['special_to_date']=$product->getSpecialToDate();
                    $arabicproduct = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(1)->load($product->getEntityId());
                    $res['name']=$arabicproduct->getName();
                    $res['arname']=$product->getName();
                    $json['items'][]=$res;
                    $json['count']= $count;
                    $json['url']= $geturl."catalogsearch/result/?q=".$quoteid;
                }


            }else{
                $json['items']=[];
                $json['count']= $count;
                $json['url']= "";
            }


            
            $resp['result'][]=$json;


        echo json_encode($resp,JSON_PARTIAL_OUTPUT_ON_ERROR);


        exit;


    }
}