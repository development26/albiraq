<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Newsletters extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

      $customerid = $this->getRequest()->get('email');

      $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      $connection = $resource->getConnection();
      $tableName = $resource->getTableName('newsletter_subscriber'); //gives table name with prefix

      $sql = "SELECT * FROM newsletter_subscriber WHERE subscriber_email = '$customerid'";
      $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.


      $count=count($result);

      //exit;

      if($count=='0'){

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            $subscriber= $objectManager->create('Magento\Newsletter\Model\SubscriberFactory'); 
            $subscriber->create()->subscribe($customerid);

            $jsons['msg']="Thank you for your Subscribe Our Newsletter";
            $jsons['error']=1;

        }else{

            $jsons['error']=0;
            $jsons['msg']="You have allready Subscribed.";


        }


        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}