<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Ajaxsentotpforloginapp extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $return = $this->_helperdata->sendLoginOTPCodeapp($this->getRequest()->get('mobile'));

        if($return=="noexist"){
        	$data = ['msg' => 'This number does not exist', 'error' => 0];
        }elseif($return=="true"){
        	$data = ['msg' => 'Otp send successfully', 'error' => 1];
        }elseif($return=="false"){
        	$data = ['msg' => 'Please enter vaild number.', 'error' => 0];
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;

    }
}