<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Emailverifyotp extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }


    public function execute()
    {

        $email = $this->getRequest()->get('email');
        $user_otp = $this->getRequest()->get('user_otp');

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('email_register_otp');


        /****************for check exist or not ***********/

        $sql = "SELECT * FROM ".$themeTable." WHERE email='".$email."' AND random_code='".$user_otp."'";
        $result=$connection->fetchAll($sql);
        $checkemil=count($result);

        foreach ($result as $key) {
            $neotp=$key['random_code'];
        }

        date_default_timezone_set('Asia/Kuwait');
	    $date=date('Y-m-d H:i:s');

        if($checkemil=="0"){

            $return="false";

        }elseif($user_otp==$neotp){

            $sqls = "UPDATE email_register_otp SET is_verify ='1', created_time='".$date."' WHERE email ='".$email."' AND random_code='".$user_otp."'";
            $connection->query($sqls);

            $return="true";
        }


        //$return = $this->_helperdata->sendLoginOTPCodeapp($this->getRequest()->get('mobile'));

		if($return=="true"){
        	$data = ['msg' => 'OTP successfully verify', 'error' => 1];
        }elseif($return=="false"){
        	$data = ['msg' => 'Please enter vaild OTP.', 'error' => 0];
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;

    }
}