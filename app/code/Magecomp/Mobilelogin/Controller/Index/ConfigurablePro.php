<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class ConfigurablePro extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $sku = $this->getRequest()->get('sku');

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
        $productModel = $objectManager->get('\Magento\Catalog\Model\Product');
        //$product = $productModel->loadByAttribute('sku', $sku);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');

        $product = $productRepository->get($sku);

        // print_r($product->getData());

        // exit;

        $res['id'] = $product->getEntityId();

        $arabicproduct = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(2)->load($res['id']);

        $res['sku'] = $product->getSku();
        $res['name'] = $product->getName();
        $res['arname'] = $arabicproduct->getName();
        $res['attribute_set_id'] = (int)$product->getAttributeSetId();
        $res['status'] = (int)$product->getStatus();
        $res['visibility'] = (int)$product->getVisibility();
        $res['type_id'] = $product->getTypeId();
        $res['created_at'] = $product->getCreatedAt();
        $res['updated_at'] = $product->getUpdatedAt();
        $res['weight'] = $product->getWeight();

        $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getId());
        $res['is_in_stock']=$productStockObj->getIsInStock();

        $products = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId());
        $images = $products->getMediaGalleryImages();
        $imgcount=count($images);

        if($imgcount!="0"){
            foreach($images as $child){
                //print_r($child);
                $img['file']=$child->getUrl();
                $img['position']=$child->getPosition();
                $res['media_gallery_entries'][]=$img;
            }   
        }else{
            $img['position']=1;
            $img['file']="https://beta.royalph.com/pub/media/catalog/product/placeholder/default/inputdataemtpy_2.jpg";
            $res['media_gallery_entries']=$img; 
        } 

        $productTypeInstance = $objectManager->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');
        $productAttributeOptions = $productTypeInstance->getConfigurableAttributesAsArray($product);

        $productTypeInstances = $product->getTypeInstance();
        $usedProducts = $productTypeInstances->getUsedProducts($product);

        foreach ($usedProducts  as $child) {
            $childs['id']= (int)$child->getId();
            $childs['price']=$child->getPrice();
            $childs['color']=$child->getColor();
        }

        foreach ($productAttributeOptions as $key => $value) {

            $resp['id']=$value['id'];
            $resp['label']=$value['label'];

            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connections = $resource->getConnection();

            $tableName = $resource->getTableName('translation');
            $attribute_information = 'Select * FROM translation WHERE string LIKE "'.$resp['label'].'"';
            $result = $connections->fetchAll($attribute_information);

            $valuec=count($result);

            foreach ($result as $keyes) {
                $getarab=$keyes['translate'];
            }

            if(!empty($valuec)){
                $resp['arlabel']=$getarab;
            }else{
                $resp['arlabel']=$resp['label'];
            }

            $resp['attribute_id']=$key;
            $resp['position']=$value['position'];

            $tmp_option = $value['values'];
            //print_r($value);
            if(count($tmp_option) > 0){
                $resp['values']=array();
                foreach ($tmp_option as $tmp){
                    //print_r($tmp);
                    $response['value']=$tmp['value_index'];
                    $response['label']=$tmp['label'];

                    $attribute_informations = 'Select * FROM eav_attribute_option_value WHERE option_id ="'.$tmp['value_index'].'" AND store_id=2';
                    $results = $connections->fetchAll($attribute_informations);
                    $getarvalue=count($results);
                    if($getarvalue!=0){
                        foreach ($results as $keyess) {
                            $resarlabel=$keyess['value'];
                        }
                    }else{
                        $resarlabel=$tmp['label'];
                    }
                    $response['arlabel']=$resarlabel;

                    //print_r($results);

                    $resp['values'][]=$response;

                }
            }
            $res['configurable_product_options'][] = $resp;
        }


        $productTypeInstances = $product->getTypeInstance();
        $usedProducts = $productTypeInstances->getUsedProducts($product);

        if(!empty($usedProducts)){

            foreach ($usedProducts  as $child) {
                
                $res['child_product_options'][] = $child->getData();
            }

        }else{
            $res['child_product_options'][] = "";
        }

        if(!empty($childs['price'])){
            $pchild=$childs['price'];
        }else{
            $pchild="0.00";
        }

        $res['price'] = (int)$pchild;

        echo json_encode($res,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;


    }
}