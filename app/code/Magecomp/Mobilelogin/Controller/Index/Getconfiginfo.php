<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Getconfiginfo extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $sku = $this->getRequest()->get('sku');
        $value = $this->getRequest()->get('value');

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
        $productModel = $objectManager->get('\Magento\Catalog\Model\Product');
        $products = $productModel->loadByAttribute('sku', $sku);

        $productTypeInstances = $products->getTypeInstance();
        $usedProducts = $productTypeInstances->getUsedProducts($products);

        foreach ($usedProducts as $keys) {
            $color=$keys['color'];
            $brand=$keys['brand'];

            if($color==$value || $brand==$value){
                $skus=$keys['sku'];
            }
        }

        $productModel = $objectManager->get('\Magento\Catalog\Model\Product');
        $product = $productModel->loadByAttribute('sku', $skus);

        //print_r($product->getData());

        $res['id'] = (int)$product->getId();
        $res['sku'] = $product->getSku();
        $res['name'] = $product->getName();
        $res['attribute_set_id'] = (int)$product->getAttributeSetId();
        $res['price'] = number_format($product->getPrice(),3);
        $res['status'] = (int)$product->getStatus();
        $res['visibility'] = (int)$product->getVisibility();
        $res['type_id'] = $product->getTypeId();
        $res['created_at'] = $product->getCreatedAt();
        $res['updated_at'] = $product->getUpdatedAt();
        $res['weight'] = $product->getWeight();

        $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($product->getId());
        $res['is_in_stock']=$productStockObj->getIsInStock();

        $productss = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getId());
        $images = $productss->getMediaGalleryImages();
        $imgcount=count($images);

        if($imgcount!="0"){
            foreach($images as $child){
                //print_r($child);
                $imfile=$child->getUrl();


                $new = str_replace('https://royalph.com/pub/media/catalog/product', '', $imfile);

                $img['file']=$new;
                $img['position']=$child->getPosition();
                $res['media_gallery_entries'][]=$img;
            }   
        }else{
            $img['position']=1;
            $img['file']="/placeholder/default/inputdataemtpy_2.jpg";
            $res['media_gallery_entries'][]=$img; 
        } 

    echo json_encode($res,JSON_PARTIAL_OUTPUT_ON_ERROR);
    exit;


    }
}