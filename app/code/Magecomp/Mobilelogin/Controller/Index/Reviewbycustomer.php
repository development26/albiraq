<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Reviewbycustomer extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

      $customerid = $this->getRequest()->get('customerid');

      $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
      $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
      $connection = $resource->getConnection();
      $tableName = $resource->getTableName('review_detail'); //gives table name with prefix

      $sql = "Select * FROM ".$tableName." WHERE customer_id=".$customerid;
      $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.


      $count=count($result);

      if($count!='0'){

        foreach($result as $key) {

            $review_id=$key['review_id'];
            $tablreview = $resource->getTableName('review'); //gives table name with prefix
            $sqlss = "SELECT created_at,entity_pk_value,status_id FROM ".$tablreview." WHERE review_id=".$review_id;

            $resultss = $connection->fetchAll($sqlss); // gives associated array, table 

            foreach ($resultss as $getstays) {
               $created_at=$getstays['created_at'];
               $status_id=$getstays['status_id'];
               $entity_pk_value=$getstays['entity_pk_value'];
            }

            if($status_id=="1"){

                $json['id']=$key['review_id'];
                $json['title']=$key['title'];
                $json['detail']=$key['detail'];
                $json['nickname']=$key['nickname'];

                $json['customer_id']=$key['customer_id'];

                $json['created_at']=$created_at;
                $json['status_id']=$status_id;
                $json['entity_pk_value']=$entity_pk_value;

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                $sellingproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($entity_pk_value);

                $arabicproduct = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(2)->load($entity_pk_value);

                $json['name'] = $sellingproduct->getName();
                $json['arname'] = $arabicproduct->getName();
                $json['sku'] = $sellingproduct->getSku();

                $json['file'] = 'https://royalph.com/pub/media/catalog/product'.$sellingproduct->getImage();

                $reviewtable = $resource->getTableName('rating_option_vote'); 
                
                $sqls = "Select * FROM ".$reviewtable." WHERE review_id=".$review_id;
                $results = $connection->fetchAll($sqls);

                $json['ratings']= array();
                foreach ($results as $revies) {
                  //print_r($revies);
                    $res['vote_id']=$revies['vote_id'];
                    $res['rating_id']=$revies['rating_id'];
                    $ratingid=$revies['rating_id'];
                    if($ratingid=="1"){
                        $res['rating_name']="Quality";
                    }elseif($ratingid=="2"){
                        $res['rating_name']="Value";
                    }elseif($ratingid=="3"){
                        $res['rating_name']="Price";
                    }
                    $res['percent']=$revies['percent'];
                    $res['value']=$revies['value'];
                    $json['ratings'][]=$res;
                }  

            $response[] =$json;

            $jsons['list']=$response;
            $jsons['error']=1;
            $jsons['msg']="Data found";

            }else{

                $jsons['list']="";
                $jsons['error']=0;
                $jsons['msg']="Data not found";

            }

        }

        }else{

            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";


        }


        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}