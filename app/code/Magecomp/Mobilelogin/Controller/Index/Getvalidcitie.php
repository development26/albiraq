<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Getvalidcitie extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $citie = $this->getRequest()->get('region');

        $orderData = array(
            "governorate" => $citie
        );

     
        $orderPayload = json_encode($orderData);
        // echo $orderPayload;
        // exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://94.128.24.216:9017/api/Majestic/GetAllAreaByGovernorate");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $orderPayload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Authorization : Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjU0NzIxMzQsImlzcyI6Imh0dHBzOi8vbG9jYWxob3N0OjQ0MzcxIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NDQzNzEifQ.UufyDRur5jui0eZXhdpmJ5ATw1RQb5fL60LTwjdr3ow',
            'accept: */*',
            'Content-Type: application/json')
        );

        if(curl_exec($ch) === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        $errors = curl_error($ch);
        $result = curl_exec($ch);
        $response = curl_exec($ch);

        $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);


        $json = json_decode($response, TRUE);

        if($json['status']['isSuccess']=="1"){

                    $responses['status']=$json['status'];

                    foreach ($json['dataValue'] as $key) {
                        $people = array("Ahmadi Governorate Desert","Janobyia Aljawakheer","Rajim Khashman","Shalehat Bneder","Shalehat Zoor","Shalehat Dba'ayeh","Shalehat Al-Nuwaiseeb","Al shadadyia Industrial","kabd Agricultural","Magwa","Al-Nuwaiseeb","Umm Al-Aish","Bhaith","Bar Al-Jahra Governorate","Bubyan Island","Warba Island","South Al Mutlaa","South Al Mutlaa 1","South Al Mutlaa 10","South Al Mutlaa 11","South Al Mutlaa 12","South Al Mutlaa 2","South Al Mutlaa 3","South Al Mutlaa 4","South Al Mutlaa 5","South Al Mutlaa 6","South Al Mutlaa 7","South Al Mutlaa 8","South Al Mutlaa 9","Jawakher Al Jahra","Rawdatain","Salmy","Shalehat Subiya","Shalehat Kazima","Al Sheqaya","Subiya","Abdally","Kazima","Kabd","Al Mutlaa","Jahra Camps","Low costs","Umm Al-Maradim Island","Umm Al-Namel Island","Auha Island","Failaka Island","Qaruh Island","Kubbar Island","Mischan Island","Shalehat Doha","Sulaibikhat Cemetery","Mina Doha","The Sea Front","South Khitan Shows");

                        $datavalue = $key['nameinEn'];

                        if (in_array($datavalue, $people)){

                        }else{
                            $respo['nameinAr']= $key['nameinAr'];
                            $respo['nameinEn']= $key['nameinEn'];
                            $respo['x']= $key['x'];
                            $respo['y']= $key['y'];

                            $responses['dataValue'][]=$respo;
                        }

                        
                    }


            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($responses);
            return $resultJson;

        }
       
        //$json = json_decode($result, TRUE);
        // print_r($result);
        // exit;

    }
}