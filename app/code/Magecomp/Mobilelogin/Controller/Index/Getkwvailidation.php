<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Getkwvailidation extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $citie = $this->getRequest()->get('city');
        $amount = $this->getRequest()->get('amount');

        $acceptCites=array("Northwest Sulaibikhat","Doha","Kaerawan","Ali Subah Al-Salem","Wafra","Khiran City","Abdullah Port","West Abdullah Al-Mubarak","Shalehat Mina Abdullah","Sabah Al-Ahmad 1","Sabah Al-Ahmad 2","Sabah Al-Ahmad 3","Sabah Al-Ahmad 4","Sabah Al-Ahmad 5","Sabah Al-Ahmad 6","شمال غرب الصليبيخات","الدوحة","علي صباح السالم","الوفرة","الخيران السكنية","شاليهات ميناء عبدالله","القيروان","غرب عبدالله المبارك","صباح الأحمد 1","صباح الأحمد 2","صباح الأحمد 3","صباح الأحمد 4","صباح الأحمد 5","صباح الأحمد 6");

        if(in_array($citie, $acceptCites)){
            if($amount<="10"){

                $jsons['msg']="10 Kwd ( Sorry, Minimum order amount for delivery for the selected address is 10 KWD')";
                $jsons['error']=0; 
            }else{
                $jsons['error']=1;
                $jsons['msg']="Data found";
            }
        }else{
                $jsons['error']=1;
                $jsons['msg']="Data found";
        }

        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}