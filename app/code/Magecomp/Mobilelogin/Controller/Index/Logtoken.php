<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Logtoken extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $data = "false";
        $mobile = $this->getRequest()->get('mobile');
        $otp = $this->getRequest()->get('otp');


        $isExist = $this->_helperdata->checkLoginOTPCode($mobile, $otp);

         if ($isExist == 1) {
              $customerData = $this->_objectManager->create('\Magento\Customer\Model\Customer');
              $customer = $customerData->getCollection()->addFieldToFilter("mobilenumber", $mobile)->getFirstItem();

              $customerId =$customer->getEntityId();
              $customeimail=$customer->getEmail();

              //print_r($customer->getData());
              
              if ($customer) {
                  $data = "true";

                    $customerToken = $this->_tokenModelFactory->create();
                    $tokenKey = $customerToken->createCustomerToken($customerId)->getToken();
              }
          }

        if($data=="true"){
            $datas = ['token' => $tokenKey, 'msg' => 'Otp verify successfully.', 'error' => 1];
          }else{
            $datas = ['token' => "", 'msg' => 'Please enter valid otp.', 'error' => 0];
          }


          $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
          $resultJson->setData($datas);
          return $resultJson;

    }
}