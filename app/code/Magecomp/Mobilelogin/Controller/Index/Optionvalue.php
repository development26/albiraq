<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Optionvalue extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $sku = $this->getRequest()->get('value');

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eav_attribute_option_value');

            $sqls = "select * FROM " . $tableName . " where option_id=".$sku;
            $results = $connection->fetchAll($sqls);
            //print_r($results);

            $checkval=count($results);

            if($checkval!="0"){

                foreach ($results as $keyes) {
                    $childstitle= $keyes['value'];
                }

                $res['title']=$childstitle;
                $res['error']=1;
                $res['success']=true;
                $res['msg']="Data found";

            }else{

                $res['title']="";
                $res['error']=0;
                $res['success']=false;
                $res['msg']="Data not found";

            }

        echo json_encode($res,JSON_PARTIAL_OUTPUT_ON_ERROR);


        exit;


    }
}