<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Getstate extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('directory_country_region'); //gives table name with prefix

        $sql = "Select * FROM " . $tableName." where country_id ='KW'";
        $result = $connection->fetchAll($sql);
        //var_dump($result);

        $count=count($result);

        if($count!='0'){


          foreach ($result as $key) {
            $json['region_id']=$key['region_id'];
            $json['getcounty']=$key['country_id'];
            $json['states_name']=$key['code'];
            $json['arstate_name']=$key['arstate_name'];
            $response[] =$json;
          }

            $jsons['list']=$response;
            $jsons['msg']="Data found";
            $jsons['error']=1;

        }else{

            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";


        }

        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}