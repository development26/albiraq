<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Subcategory extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $data = "false";
        $parentCatId = $this->getRequest()->get('parentCatId');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // instance of object manager

        $categoryRepository = $objectManager->create('Magento\Catalog\Model\CategoryRepository');
        $parentcategories = $categoryRepository->get($parentCatId);
        $categories = $parentcategories->getChildrenCategories();

        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');


        $count=count($categories);

        if($count!='0'){

          foreach ($categories as $key => $value) {
              $res['entity_id']=$value->getEntityId();
              $cateid=$value->getEntityId();
              $res['arname']=$value->getName();

              $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
              $cateinstance = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
              $allcategoryproduct = $cateinstance->create()->load($cateid)->getProductCollection()->addAttributeToSelect('*');
              $res['count'] = $allcategoryproduct->count();

              $_category = $objectManager->create('Magento\Catalog\Model\Category')->setStoreId(1)->load($cateid);
              $res['name']=$_category->getName();
              $rimage= $_category->getImageUrl();

              if($rimage!=""){
                $res['image']=$storeManager->getStore()->getBaseUrl()."".$rimage;
              }else{
                $res['image']=$storeManager->getStore()->getBaseUrl().'pub/media/catalog/category/m.png';
              }

              $response[] =$res;
          }
            $jsons['list']=$response;
            $jsons['msg']="Category list found";
            $jsons['error']=1;
        }elseif($count=='0'){

            $res['entity_id']=$parentcategories->getEntityId();
            $cateid=$parentcategories->getEntityId();
            $res['arname']=$parentcategories->getName();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            $cateinstance = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
            $allcategoryproduct = $cateinstance->create()->load($cateid)->getProductCollection()->addAttributeToSelect('*');
            $res['count'] = $allcategoryproduct->count();

            $_category = $objectManager->create('Magento\Catalog\Model\Category')->setStoreId(1)->load($cateid);
            $res['name']=$_category->getName();
            $rimage= $_category->getImageUrl();

            if($rimage=="/pub/media/catalog/category/offers_2.gif"){
              $res['image']=$storeManager->getStore()->getBaseUrl().'/pub/media/catalog/category/offers_2.gif';
            }elseif($rimage!=""){
              $res['image']=$rimage;
            }else{
              $res['image']=$storeManager->getStore()->getBaseUrl().'pub/media/catalog/category/m.png';
            }

            $response[] =$res;

            $jsons['list']=$response;
            $jsons['msg']="Category list found";
            $jsons['error']=1;

        }else{
            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";
        }


        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}