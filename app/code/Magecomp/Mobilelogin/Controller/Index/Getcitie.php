<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Getcitie extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {

        $region = $this->getRequest()->get('region');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('magecomp_cities'); //gives table name with prefix

        $sql = "Select * FROM " . $tableName." where states_name ='".$region."'";
        $result = $connection->fetchAll($sql);
        //var_dump($result);

        $count=count($result);

        if($count!='0'){


          foreach ($result as $key) {
            $json['states_name']=$key['states_name'];
            $json['cities_name']=$key['cities_name'];
            $json['nameinAr']=$key['nameinAr'];
            $response[] =$json;
          }

            $jsons['list']=$response;
            $jsons['msg']="Data found";
            $jsons['error']=1;

        }else{

            $jsons['list']="";
            $jsons['error']=0;
            $jsons['msg']="Data not found";


        }

        echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}