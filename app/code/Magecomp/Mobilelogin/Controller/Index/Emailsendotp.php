<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Emailsendotp extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData
    )
    {

        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }


    public function generateRandomString()
    {
        $length = "4";
        if("N" == "N"){
            $randomString = substr(str_shuffle("0123456789"), 0, $length);
        }
        else{
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);  
        }

        return $randomString;
    }

    public function execute()
    {

        $email = $this->getRequest()->get('email');
        $emailes = $this->getRequest()->get('email');

        $randomCode = $this->generateRandomString();

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('email_register_otp');

        /*****************check is verify or not *************/

        $objectManager=\Magento\Framework\App\ObjectManager::getInstance();
        $CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
        $CustomerModel->setWebsiteId(1);

        $CustomerModel->loadByEmail($email);
        $userId = $CustomerModel->getId();

        if ($userId) {

            $data = ['msg' => 'This email already exist with another Account.', 'error' => 0];
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($data);
            return $resultJson;
        }


        // $sqls = "SELECT * FROM ".$themeTable." WHERE email='".$email."' AND is_verify=1";
        // $checkverify=$connection->fetchAll($sqls);

        // $checkverify=count($checkverify);

        // if($checkverify=="1"){

        // 	$data = ['msg' => 'This email already exist with another Account.', 'error' => 0];
        // 	$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
	       //  $resultJson->setData($data);
	       //  return $resultJson;
        // }

        /****************for check exist or not ***********/

        $sql = "SELECT * FROM ".$themeTable." WHERE email='".$email."'";
        $result=$connection->fetchAll($sql);
        $checkemil=count($result);

        date_default_timezone_set('Asia/Kuwait');
	    $date=date('Y-m-d H:i:s');

        if($checkemil=="0"){

	        $sql = "INSERT INTO " . $themeTable . "(id, random_code, created_time, email, is_verify) VALUES ('', '".$randomCode."', '".$date."', '".$email."', 0)";
	        $connection->query($sql);

	        $return="true";

        }else{

        	$sqls = "UPDATE email_register_otp SET random_code ='".$randomCode."', created_time='".$date."' WHERE email ='".$email."'";
            $connection->query($sqls);

            $return="true";

        }

        if($return=="true"){
                            /************************send email ************************/

                    $msg='<div align="center" class="header" style="font-family: FuturaBookC,sans-serif;">
                            <img src="https://albiraq.kasme.com/media/logo/stores/1/Albiraq_final_Logo-transparent.png" style="width:300px;">
                            <h1  style="text-align: center; font-weight: 500;color: #383838;font-size: 20px;border: 1px solid #8CBB3F; border-left: 0;border-right: 0;margin: 15px -10px; padding: 3px;">Albiraq Pharmacy One time password</h1>
                        </div>

                    <p style="line-height:35px;font-size: 16px;font-family: FuturaBookC,sans-serif;">Hello Customer,</b></b></p>

                    <p style="line-height:30px;font-size:14px;font-family: FuturaBookC,sans-serif;margin:0;width:100%">Thank you for Chosseing us.</p>
                      
                    <p style="line-height:30px;font-size:14px;font-family: FuturaBookC,sans-serif;margin:0;width:100%">Your one time password is <strong style="font-size: 20px;">'.$randomCode.'</strong>.</p>
                      
                    <p style="line-height:30px;font-size:14px;font-family: FuturaBookC,sans-serif;margin:0;width:100%"> After registration you will buy any medicine whenever you wish!</p>
                     
                    <p style="line-height:30px;font-size:14px;font-family: FuturaBookC,sans-serif;margin:0;width:100%"> Happy Shopping!</p>
                     
                    <p style="line-height:30px;font-size:14px;font-family: FuturaBookC,sans-serif;margin:0;width:100%"></p>

                    <br>
                    <br>
                    <h3 style="font-family: FuturaBookC,sans-serif;">Thank you,</h3> 
                    <h3 style="font-family: FuturaBookC,sans-serif;">Albiraq Pharmacy<h3>';


                    //require_once '../vendor/autoload.php';
                    require '../vendor/autoload.php'; // If you're using Composer (recommended)
                    $email = new \SendGrid\Mail\Mail();
                    $email->setFrom("albiraqpharmacykuwait@gmail.com", "Albiraq Pharmacy");
                    $email->setSubject("One time password form you registration.");
                    $email->addTo($emailes, "name");
                    $email->addContent(
                        "text/html", $msg
                    );
                    $sendgrid = new \SendGrid('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');
                    try {
                        $response = $sendgrid->send($email);
                        $response->statusCode();
                    } catch (Exception $e) {
                        $e->getMessage();
                    }
        }


        //$return = $this->_helperdata->sendLoginOTPCodeapp($this->getRequest()->get('mobile'));

		if($return=="true"){
        	$data = ['msg' => 'Otp send successfully on your Email addess.', 'error' => 1];
        }elseif($return=="false"){
        	$data = ['msg' => 'Please enter vaild Email Id.', 'error' => 0];
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;

    }
}