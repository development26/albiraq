<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Inventory extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {


        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        // $collection = $productCollection->addAttributeToSelect('*')
        //             ->load();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $securedArea = $objectManager->get('\Magento\Framework\Registry');
        $securedArea->register('isSecureArea', true);

        $om = $objectManager->get('Magento\Framework\App\State');
        //$om->setAreaCode('global');


        $dbhost = '62.150.30.82';
        $dbuser = 'linkuser';
        $dbpass = 'pas721LD4e*!';
        $dbname = 'dblink';
        $mysqli = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
         
        if($mysqli->connect_errno ) {
            printf("Connect failed: %s<br />", $mysqli->connect_error);
            exit();
        }

        $sql = "SELECT Region, Code, Qty FROM Stock";
        $result = $mysqli->query($sql);


        $con = mysqli_connect('localhost','root','smr@Spartan1','royal_pharmacydb');
            
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                   //printf("Id: %s, Title: %s, Author: %s<br />", 
                    $dbreg=$row["Region"];
                    $dbcode= $row["Code"];
                    $dbqty=$row["Qty"]; 

                    if($dbreg=="Ahmadi"){
                        $dbreges="Ahmadi";
                    }elseif($dbreg=="Farwaniya"){
                        $dbreges="Farwaniya";
                    }elseif($dbreg=="Hawally"){
                        $dbreges="Hawalli";
                    }elseif($dbreg=="Jahra"){
                        $dbreges="Jahra";
                    }elseif($dbreg=="Mubarak Al Kabeer"){
                        $dbreges="Mubarak-Al-Kabeer";
                    }elseif($dbreg=="Asma"){
                        $dbreges="asma";
                    }

                $query = "UPDATE inventory_source_item SET quantity='".$dbqty."' WHERE source_code='".$dbreges."' AND sku='".$dbcode."'";
                $res = mysqli_query($con,$query);

                }
            } else {
                printf('No record found.<br />');
            }
            mysqli_free_result($result);
            $mysqli->close();
            
        //printf('Updated successfully.<br />');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('cron_update'); //gives table name with prefix

        //Insert Data into table
        date_default_timezone_set('Asia/Kuwait');
        $date=date('Y-m-d H:i:s');

        $sql = "Insert Into " . $tableName . " (id, cronname, cron_update, date) Values ('','Inventory','Success','".$date."')";
        $connection->query($sql);

        exit;

    }
}