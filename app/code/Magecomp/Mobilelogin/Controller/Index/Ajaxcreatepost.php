<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Helper\Address;
use Magento\Framework\UrlFactory;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Customer\Model\Registration;
use Magento\Framework\Escaper;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Controller\ResultFactory;

class Ajaxcreatepost extends \Magento\Framework\App\Action\Action
{
    protected $accountManagement;
    protected $addressHelper;
    protected $formFactory;
    protected $subscriberFactory;
    protected $regionDataFactory;
    protected $addressDataFactory;
    protected $registration;
    protected $customerDataFactory;
    protected $customerUrl;
    protected $escaper;
    protected $customerExtractor;
    protected $urlModel;
    protected $dataObjectHelper;
    protected $session;
    private $accountRedirect;

    public function __construct(
        Context $context,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        Address $addressHelper,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerInterfaceFactory $customerDataFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        DataObjectHelper $dataObjectHelper,
        AccountRedirect $accountRedirect
    )
    {
        $this->session = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->accountManagement = $accountManagement;
        $this->addressHelper = $addressHelper;
        $this->formFactory = $formFactory;
        $this->subscriberFactory = $subscriberFactory;
        $this->regionDataFactory = $regionDataFactory;
        $this->addressDataFactory = $addressDataFactory;
        $this->customerDataFactory = $customerDataFactory;
        $this->customerUrl = $customerUrl;
        $this->registration = $registration;
        $this->escaper = $escaper;
        $this->customerExtractor = $customerExtractor;
        $this->urlModel = $urlFactory->create();
        $this->dataObjectHelper = $dataObjectHelper;
        $this->accountRedirect = $accountRedirect;
        parent::__construct($context);
    }

    protected function extractAddress()
    {
        if (!$this->getRequest()->getPost('create_address')) {
            return null;
        }
        $addressForm = $this->formFactory->create('customer_address', 'customer_register_address');
        $allowedAttributes = $addressForm->getAllowedAttributes();
        $addressData = array();
        $regionDataObject = $this->regionDataFactory->create();
        foreach ($allowedAttributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $value = $this->getRequest()->getParam($attributeCode);
            if ($value === null) {
                continue;
            }
            switch ($attributeCode) {
                case 'region_id':
                    $regionDataObject->setRegionId($value);
                    break;
                case 'region':
                    $regionDataObject->setRegion($value);
                    break;
                default:
                    $addressData[$attributeCode] = $value;
            }
        }
        $addressDataObject = $this->addressDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $addressDataObject,
            $addressData,
            '\Magento\Customer\Api\Data\AddressInterface'
        );
        $addressDataObject->setRegion($regionDataObject);

        $addressDataObject->setIsDefaultBilling(
            $this->getRequest()->getParam('default_billing', false)
        )->setIsDefaultShipping(
            $this->getRequest()->getParam('default_shipping', false)
        );
        return $addressDataObject;
    }

    public function execute()
    {
        $data = array();
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn() || !$this->registration->isAllowed()) {
            $resultRedirect->setPath('*/*/');
            return "false";
        }


        $customformKey = $this->getRequest()->getParam('form_key');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($customformKey == $objectManager->create('\Magento\Framework\Data\Form\FormKey')->getFormKey()) {


            $email = $this->getRequest()->getParam('email');

            if (strpos($email, "@rambler.ru") || strpos($email, "@mail.ru") || strpos($email, "@sf-express.com") || strpos($email, "@qqlong.com") || strpos($email, "@kinohit.win") || strpos($email, "@sohu.com") || strpos($email, "@21cn.com") || strpos($email, "@yeah.net") || strpos($email, "@koreamail.com") || strpos($email, "@aliyun.com") ||strpos($email, "@icload.com") || strpos($email, "@sogou.com") || strpos($email, "@vip.qq.com") || strpos($email, "@sina.cn") || strpos($email, "@189.cn") || strpos($email, "@wo.com.cn") || strpos($email, "@qq.cn") || strpos($email, "@sina.com.cn") || strpos($email, "@126.cn") || strpos($email, "@yahoo.com.cn") || strpos($email, "@3g.cn") || strpos($email, "@163.com") || strpos($email, "@167.com") || strpos($email, "@139.com") || strpos($email, "@126.com") || strpos($email, "@130.com") || strpos($email, "@144.com") || strpos($email, "@123.com") || strpos($email, "@ca800.com") || strpos($email, "@168.com") || strpos($email, "@188.com") || strpos($email, "@1974.com") || strpos($email, "@qq.co") || strpos($email, "@sina.com") || strpos($email, "@qq.con") || strpos($email, "@QQ.come") || strpos($email, "@yandex.com") || strpos($email, "@5ol.com") || strpos($email, "@yeat.net") || strpos($email, "@yahoo.cn") || strpos($email, "@lenta.ru") || strpos($email, "@autorambler.ru")) {
                $message = __(
                    'We can not allow register with this email.'
                );
                $data['success'] = "false";
                $data['message'] = $message;
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($data);
                return $resultJson;
            }

            if (strpos($email, "@qq.com")) {
                $message = __(
                    'We can not allow register with this email.'
                );
                $data['success'] = "false";
                $data['message'] = $message;
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($data);
                return $resultJson;
            }

            $mobilenumber = $this->getRequest()->getParam('mobilenumber');

            if (empty($mobilenumber)) {
                $message = __(
                    'We can\'t save the customer.'
                );
                $data['success'] = "false";
                $data['message'] = $message;
                $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
                $resultJson->setData($data);
                return $resultJson;
            }


            $this->session->regenerateId();
            try {
                $address = $this->extractAddress();
                $addresses = $address === null ? [] : [$address];

                $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
                $customer->setAddresses($addresses);

                $password = $this->getRequest()->getParam('password');
                $confirmation = $this->getRequest()->getParam('password_confirmation');
                $redirectUrl = $this->session->getBeforeAuthUrl();

                $mobilenumber = $this->getRequest()->getParam('mobilenumber');

                $email = $this->getRequest()->getParam('email');

                $this->checkPasswordConfirmation($password, $confirmation);
                $customer = $this->accountManagement
                    ->createAccount($customer, $password, $redirectUrl);
                $customer->setCustomAttribute('mobilenumber', $mobilenumber);
                $this->_eventManager->dispatch(
                    'customer_register_success',
                    ['account_controller' => $this, 'customer' => $customer]
                );
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->messageManager->addSuccess($this->getSuccessMessage());

                $objectManager =  \Magento\Framework\App\ObjectManager::getInstance(); 
                $storeManager  = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $storeId       = $storeManager->getStore()->getStoreId();
                $customerModel = $this->_objectManager->create('Magento\Customer\Model\Customer');
                $customerModel->setWebsiteId($storeId);
                $customerModel->loadByEmail($email);
                $userId = $customerModel->getId();

                $data['success'] = "true";
                $data['redirect'] = $this->urlModel->getUrl('customer/account/');
            } catch (StateException $e) {
                $url = $this->urlModel->getUrl('customer/account/forgotpassword');
                // @codingStandardsIgnoreStart
                $message = __(
                    'There is already an account with this email address.',
                    $url
                );
                $data['success'] = "false";
                $data['message'] = $message;
            } catch (InputException $e) {
                $data['success'] = "false";
                $data['message'] = $e->getMessage();
            } catch (LocalizedException $e) {
                $data['success'] = "false";
                $data['message'] = $e->getMessage();
            } catch (\Exception $e) {
                $data['success'] = "false";
                $data['message'] = 'We can\'t save the customer';
            }

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($data);
            return $resultJson;

        }

    }

    protected function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    protected function getSuccessMessage()
    {
        if ($this->addressHelper->isVatValidationEnabled()) {
            if ($this->addressHelper->getTaxCalculationAddressType() == Address::TYPE_SHIPPING) {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your shipping address for proper VAT calculation.',
                    $this->urlModel->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            } else {
                // @codingStandardsIgnoreStart
                $message = __(
                    'If you are a registered VAT customer, please <a href="%1">click here</a> to enter your billing address for proper VAT calculation.',
                    $this->urlModel->getUrl('customer/address/edit')
                );
                // @codingStandardsIgnoreEnd
            }
        } else {
            $message = __('Thank you for registering with %1.', $this->storeManager->getStore()->getFrontendName());
        }
        return $message;
    }
}