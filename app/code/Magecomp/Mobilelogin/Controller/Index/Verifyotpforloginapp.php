<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;

class Verifyotpforloginapp extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;
    protected $session;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        \Magecomp\Mobilelogin\Helper\Data $helperData,
        Session $customerSession

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->session = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
      $flag = $this->getRequest()->get('processId');
        if($flag == 2){ 
              $data = "false"; $isExist = 0;
              $mobile = $this->getRequest()->get('mobile');
              $otp = $this->getRequest()->get('otp');
              $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
              $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
              $connection = $resource->getConnection();
              $sql = "SELECT * FROM  sms_register_otp WHERE mobile =".$mobile." AND random_code=".$otp;
              $res = $connection->fetchAll($sql);
              if(!empty($res)){
                $data = "true";
              }
              $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
              $resultJson->setData($data);
              return $resultJson;

        }
        else{
          $data = "false";
          $mobile = $this->getRequest()->get('mobile');
          $otp = $this->getRequest()->get('otp');
          $isExist = $this->_helperdata->checkLoginOTPCode($mobile, $otp);
          if ($isExist == 1) {
              $customerData = $this->_objectManager->create('\Magento\Customer\Model\Customer');
              $customer = $customerData->getCollection()->addFieldToFilter("mobilenumber", $mobile)->getFirstItem();

              $customerid =$customer->getEntityId();
              $customeimail=$customer->getEmail();

              //print_r($customer->getData());
              
              if ($customer) {
                  $this->session->setCustomerAsLoggedIn($customer);
                  $this->session->regenerateId();

                  $data = "true";
                  if ($this->_helperdata->isEnableLoginEmail()) {
                      $this->_helperdata->sendMail($_SERVER['REMOTE_ADDR'], $customer->getEmail(), $_SERVER['HTTP_USER_AGENT']);
                  }
              }
          }


          if($data=="true"){
            $datas = ['msg' => 'Otp verify successfully.', 'error' => 1];
          }else{
            $datas = ['msg' => 'Please enter valid otp.', 'error' => 0];
          }


          $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
          $resultJson->setData($datas);
          return $resultJson;
        }
    }
}
