<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Homedatapost extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        //$returnVal = $this->_helperdata->sendOTPCodemobile($data['mobile']);

        $returnVal="exist";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $geturl=$this->_storeManager->getStore()->getBaseUrl();

                /***********get category details 8*************/

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connections = $resource->getConnection();
        
        $catId = 2;  //Parent Category ID
        $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
        $subCats = $subCategory->getChildrenCategories();
        //$_helper = $this->helper('Magento\Catalog\Helper\Output');
        
        foreach ($subCats as $subcat) {
            $getcatid=$subcat->getName();

            $tableName = $resource->getTableName('translation');
            $attribute_information = 'Select * FROM translation WHERE string LIKE "'.$getcatid.'"';
            $result = $connections->fetchAll($attribute_information);

            foreach ($result as $key) {
                $getarab=$key['translate'];
            }

            $_category = $objectManager->create('Magento\Catalog\Model\Category')->setStoreId(1)->load($subcat->getId());
            $cate1['subcaturl'] = $subcat->getUrl();
            $cate1['id']=(int)$subcat->getId();
            // $_imgHtml = '';

            // if ($_imgUrl = $_category->getImageUrl()) {
            //     $cate1['image'] = $geturl.''.$_imgUrl;
            // } 

            //$_imgHtml = '';

            $_imgUrl = $_category->getImageUrl();
            if($_imgUrl=="https://albiraq.kasme.com/pub/media/catalog/category/offers_2.gif"){
                $cate1['image'] = "https://albiraq.kasme.com/pub/media/catalog/category/offers_2.gif";
            }elseif(empty($_imgUrl)){
                $cate1['image'] = "https://albiraq.kasme.com/pub/media/wysiwyg/catimg.jpeg";
            }else{
                $cate1['image'] = $_imgUrl;
            }

            $cate1['arname']=$subcat->getName();
            $cate1['name']=$_category->getName();
            $json['category-list'][]=$cate1;
        } 
          
        /********* end category details **************/

        /***********get Banner details details 8*************/

        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
        $banner1 = $connection->fetchAll("SELECT * FROM mageplaza_bannerslider_banner Where status='1'");

        foreach ($banner1 as $banners) {
            $bann['name']=$banners['name'];
            $bann['image']=$geturl.'pub/media/mageplaza/bannerslider/banner/image/'.$banners['image'];
            $banurl=$banners['url_banner'];
            if(!empty($banurl)){
                $bann['url_banner']=$banurl;
            }else{
                $bann['url_banner']="";
            }
            $bann['status']=$banners['status'];
            $json['banner-list'][]=$bann;
        }

        /***********end banner details 8*************/

        /********* Bestsellers Product product **************/
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        /*$sqlmask = "SELECT `e`.*, SUM(order_items.qty_ordered) AS `ordered_qty`, `order_items`.`product_id`, `cat_index`.`position` AS `cat_index_position`, `price_index`.`price`, `price_index`.`tax_class_id`, `price_index`.`final_price`, IF(price_index.tier_price IS NOT NULL, LEAST(price_index.min_price, price_index.tier_price), price_index.min_price) AS `minimal_price`, `price_index`.`min_price`, `price_index`.`max_price`, `price_index`.`tier_price` FROM `catalog_product_entity` AS `e`
 INNER JOIN `sales_order_item` AS `order_items`
 INNER JOIN `catalog_category_product_index_store1` AS `cat_index` ON cat_index.product_id=e.entity_id AND cat_index.store_id=1 AND cat_index.visibility IN(2,
4) AND cat_index.category_id=2
 INNER JOIN `catalog_product_index_price` AS `price_index` ON price_index.entity_id = e.entity_id AND price_index.customer_group_id = 0 AND price_index.website_id = '1'
 INNER JOIN `sales_order` AS `order` ON `order`.entity_id = order_items.order_id AND `order`.state <> 'canceled' WHERE (e.entity_id = order_items.product_id and parent_item_id IS NULL) GROUP BY `order_items`.`product_id` HAVING (SUM(order_items.qty_ordered) > 0) ORDER BY `ordered_qty` DESC
 LIMIT 20";*/

        $objectManager   = \Magento\Framework\App\ObjectManager::getInstance();
        $visibleProducts = $objectManager->create('\Magento\Catalog\Model\Product\Visibility')->getVisibleInCatalogIds();
        $collection = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Collection')->setVisibility($visibleProducts);
        $now = date('Y-m-d H:i:s');
        $collection->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('image')
            ->addAttributeToSelect('small_image')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSelect('news_from_date')
            ->addAttributeToSelect('news_to_date')
            ->addAttributeToSelect('short_description')
            ->addAttributeToSelect('special_from_date')
            ->addAttributeToSelect('special_to_date')
            ->addAttributeToFilter('special_price', ['neq' => ''])
            ->addAttributeToFilter('special_from_date', ['lteq' => date('Y-m-d  H:i:s', strtotime($now))])
            ->addAttributeToFilter('special_to_date', ['gteq' => date('Y-m-d  H:i:s', strtotime($now))])
            ->addAttributeToFilter('is_saleable', ['eq' => 1], 'left');

        //$resultmask = $connection->fetchAll($sqlmask);

        //print_r($resultmask);

            //var_dump($collection->getData());

        foreach ($collection as $items){



            if($items['type_id']=="simple"){

                $prodid = $items['entity_id'];
                $sellingproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($prodid);
                $arabicproduct = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(1)->load($prodid);
                $bestproduct_id = $sellingproduct->getEntityId();
                if($bestproduct_id==$prodid){
                    $bestselling['id'] = (int)$sellingproduct->getEntityId();
                    $bestselling['arname'] = $sellingproduct->getName();
                    $bestselling['name'] = $arabicproduct->getName();
                    $bestselling['type_id'] = $arabicproduct->getTypeId();
                    $bestselling['sku'] = $sellingproduct->getSku();
                    //$bestselling['price'] = number_format($sellingproduct->getPrice(),3);
                    $bestselling['price'] = number_format($sellingproduct->getPrice(),3);

                    $sperical = number_format($sellingproduct->getSpecialPrice(),3);

                    if($sperical!=""){
                        $spericalp=$sperical;
                    }else{
                        $spericalp=0.00;
                    }

                    $bestselling['special_price']=$spericalp;
                    $special_from = $arabicproduct->getSpecialFromDate();
                    if($special_from!=""){
                        $bestselling['special_from_date'] = $arabicproduct->getSpecialFromDate();
                    }else{
                        $bestselling['special_from_date'] = "";
                    }

                    $special_to = $arabicproduct->getSpecialToDate();

                    if($special_to!=""){
                        $bestselling['special_to_date'] = $arabicproduct->getSpecialToDate();
                    }else{
                        $bestselling['special_to_date'] = "";
                    }


                    $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($bestselling['id']);
                    $bestselling['is_in_stock']=$productStockObj->getIsInStock();

                    // foreach ($productStockObj as $stockkey) {
                    //     echo $bestselling['status']=$stockkey['is_in_stock'];
                    // }

                    $bestselling['file'] = $geturl.'pub/media/catalog/product'.$sellingproduct->getImage();
                    $json['dealsandoffers'][]=$bestselling;

                }
            }
        }

        /********* Bestsellers Product product **************/

        //print_r($collection->getData());


        if($returnVal=="exist"){
            $data = ['list'=>$json, 'msg' => 'Data found.', 'error' => '1'];
        }else{
            $data = ['msg' => 'No data found.', 'error' => '0'];
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;


    }
}