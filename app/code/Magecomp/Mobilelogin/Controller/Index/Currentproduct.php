<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;

class Currentproduct extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        MagecompHelper $helperData,
        \Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->_tokenModelFactory = $tokenModelFactory;
        parent::__construct($context);
    }

    public function execute()
    {


        // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        // $collection = $productCollection->addAttributeToSelect('*')
        //             ->load();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sqls = "SELECT * FROM catalog_product_flat_1 WHERE type_id ='simple'";
        $productski = $connection->fetchAll($sqls);

        //echo "products count == ".count($productski)."<br>";

        foreach ($productski as $product){
                $sku = $product['sku'];
                $id = $product['entity_id'];

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();

                $sql = "SELECT * FROM inventory_source_item WHERE sku = '".$sku."'";
                
                $result = $connection->fetchAll($sql);

                $qty=0;
                $qtys=0;
                foreach ($result as $key) {
                    $getsource=$key['source_code'];
                    if($getsource=="default" || $getsource=="Capital"){
                        $qtys+=$key['quantity'];
                    }else{
                        $qty+=$key['quantity'];
                    }
                }



                if($qty!="0"){
                    $status="1";
                }else{
                    $status="0";
                }

                //echo $qty."<br>";

                $tableName = $resource->getTableName('inventory_source_item');
                $sqls = "Update " . $tableName . " Set quantity = ".$qty.", status = ".$status." where sku = '".$sku."' AND source_code='default'";
                $connection->query($sqls);

                $tableName = $resource->getTableName('cataloginventory_stock_item');
                $sqlss = "Update " . $tableName . " Set qty = ".$qty." where product_id = '".$id."'";
                $connection->query($sqlss);

        } 

        //echo "done";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('cron_update'); //gives table name with prefix

        //Insert Data into table
        date_default_timezone_set('Asia/Kuwait');
        $date=date('Y-m-d H:i:s');

        $sql = "Insert Into " . $tableName . " (id, cronname, cron_update, date) Values ('','Inventory update','Success','".$date."')";
        $connection->query($sql);

        //echo json_encode($jsons,JSON_PARTIAL_OUTPUT_ON_ERROR);
        exit;

    }
}