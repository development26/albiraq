<?php
namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magecomp\Mobilelogin\Model\LoginotpmodelFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Model\Session;

class Ajaxverifyotpforloginapp extends \Magento\Framework\App\Action\Action
{
    protected $_modelLoginOtpFactory;
    public $_helperdata;
    protected $session;
    protected $repository;

    public function __construct(
        Context $context,
        LoginotpmodelFactory $modelLoginOtpFactory,
        \Magecomp\Mobilelogin\Helper\Data $helperData,
        Session $customerSession

    )
    {
        $this->_modelLoginOtpFactory = $modelLoginOtpFactory;
        $this->_helperdata = $helperData;
        $this->session = $customerSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = "false"; $isExist = 0;
        $mobile = $this->getRequest()->get('mobile');
        $otp = $this->getRequest()->get('otp');
        $smrId = $this->getRequest()->get('userId');
        //exit;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT * FROM  sms_register_otp WHERE mobile =".$mobile." AND random_code=".$otp;
        $res = $connection->fetchAll($sql);
        if(!empty($res)){
          $data = "true";
        }

        if($data == "true"){

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('sms_register_otp'); //gives table name with prefix

            $sql = "UPDATE " . $tableName . " SET is_verify = 1 where mobile =".$mobile;
            $connection->query($sql);

            $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
            $customer = $customerFactory->load($smrId);

            $sqls = "SELECT * FROM  customer_entity_varchar WHERE entity_id =".$smrId." AND attribute_id=150";
            $responsenum = $connection->fetchAll($sqls);

            $checknumber=count($responsenum);
            /***********updaten number **********/

            if($checknumber=="0"){
                $tableName = $resource->getTableName('customer_entity_varchar'); //gives table name with prefix
                //$sql = "UPDATE " . $tableName . " SET value = ".$mobile." where entity_id =".$smrId;
                $sql = "INSERT INTO " . $tableName . "(attribute_id, entity_id, value) VALUES ('150', ".$smrId.", ".$mobile.")";
                $connection->query($sql);
            }else{
                $tableName = $resource->getTableName('customer_entity_varchar'); //gives table name with prefix
                $sql = "UPDATE " . $tableName . " SET value = ".$mobile." where entity_id =".$smrId;
                $connection->query($sql);
            }

            
            /***********updaten number **********/

            /***********for customer phone number update **********/

            $billingAddressId = $customer->getDefaultBilling();
            $shippingAddressId = $customer->getDefaultShipping();
            if(!empty($shippingAddressId)){
            $addressRepository = $objectManager->create('Magento\Customer\Api\AddressRepositoryInterface');
            $address = $addressRepository->getById($billingAddressId);
            $address->setTelephone($mobile);
            $addressRepository->save($address);
            $address = $addressRepository->getById($shippingAddressId);
            $address->setTelephone($mobile);
            $addressRepository->save($address);

          }

        }

          if($data=="true"){
              $datas = ['msg' => 'Mobile number update successfully.', 'error' => 1];
          }elseif($data=="false"){
              $datas = ['msg' => 'Please enter vaild Otp.', 'error' => 0];
          }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($datas);
        return $resultJson;

    }
}
