<?php

namespace Magecomp\Mobilelogin\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magecomp\Mobilelogin\Helper\Data as MagecompHelper;
use Magento\Framework\Controller\ResultFactory;

class Arabichomedatapost extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $jsonResultFactory;
    protected $session;
    protected $formKeyValidator;
    public $_storeManager;
    public $_helperdata;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $jsonResultFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        StoreManagerInterface $storeManager,
        MagecompHelper $helperData
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        $this->_storeManager = $storeManager;
        $this->_helperdata = $helperData;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        //$returnVal = $this->_helperdata->sendOTPCodemobile($data['mobile']);

        $returnVal="exist";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $geturl=$this->_storeManager->getStore()->getBaseUrl();

        /***********get Banner details details 8*************/

        $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
        $banner1 = $connection->fetchAll("SELECT * FROM mageplaza_bannerslider_banner Where status='1'");

        foreach ($banner1 as $banners) {
            $bann['name']=$banners['name'];
            $bann['image']=$geturl.'pub/media/mageplaza/bannerslider/banner/image/'.$banners['image'];
            $banurl=$banners['url_banner'];
            if(!empty($banurl)){
                $bann['url_banner']=$banurl;
            }else{
                $bann['url_banner']="";
            }
            $bann['status']=$banners['status'];
            $json['banner-list'][]=$bann;
        }

        /***********end banner details 8*************/

        /***********get category details 8*************/

        
        $catId = 2;  //Parent Category ID
        $subCategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
        $subCats = $subCategory->getChildrenCategories();
        //$_helper = $this->helper('Magento\Catalog\Helper\Output');
        
        foreach ($subCats as $subcat) {
            $_category = $objectManager->create('Magento\Catalog\Model\Category')->load($subcat->getId());
            $cate1['subcaturl'] = $subcat->getUrl();
            $cate1['id']=(int)$subcat->getId();
            $_imgHtml = '';

            if ($_imgUrl = $_category->getImageUrl()) {
                $cate1['image'] = $geturl.''.$_imgUrl;
            } 

            $cate1['name']=$subcat->getName();
            $json['category-list'][]=$cate1;
        } 
          
        /********* end category details **************/

        /********* Bestsellers Product product **************/
        $objectManagers = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollections = $objectManagers->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory'); 
        $collections = $productCollections->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection'); 
        $collections->setPeriod('year');

        foreach ($collections as $items){
            $prodid = $items['product_id'];
            $sellingproduct = $objectManager->create('Magento\Catalog\Model\Product')->load($prodid);
            $bestproduct_id = $sellingproduct->getEntityId();
            if($bestproduct_id==$prodid){
                $bestselling['id'] = (int)$sellingproduct->getEntityId();
                $bestselling['name'] = $sellingproduct->getName();
                $bestselling['sku'] = $sellingproduct->getSku();
                //$bestselling['price'] = number_format($sellingproduct->getPrice(),3);
                $bestselling['price'] = floatval($sellingproduct->getPrice());

                $sperical = number_format($sellingproduct->getSpecialPrice());

                if($sperical!=""){
                	$spericalp=$sperical;
                }else{
                	$spericalp="0.00";
                }

                $bestselling['special_price']=(int)$spericalp;

                $bestselling['file'] = $geturl.'pub/media/catalog/product'.$sellingproduct->getImage();
                $json['bestselling'][]=$bestselling;
            }
        }

        /********* Bestsellers Product product **************/

        /********* Insurance detaisl product **************/

        $attributeCode = 'insuranc_companys';
        $entityType = 'customer';

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $attributeInfo = $objectManager->get(\Magento\Eav\Model\Entity\Attribute::class)
                                       ->loadByCode($entityType, $attributeCode);

        $attributeId = $attributeInfo->getAttributeId();
        $attributeOptionAll = $objectManager->get(\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection::class)
                                            ->setPositionOrder('asc')
                                            ->setAttributeFilter($attributeId)
                                            ->setStoreFilter()
                                            ->load();

        foreach ($attributeOptionAll as $key) {
            $insu['insurance']=$key['default_value'];
            $insu['insurance_image']=$geturl.'pub/media/mageplaza/bannerslider/banner/image/'.str_replace(' ', '', $insu['insurance']).'.png';
            $json['insurance-list'][]=$insu;

        }

        /********* Insurance detaisl product **************/

        /********* get feature product **************/

        $productCollection = $objectManager
            ->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $collection = $productCollection->create()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('sm_featured', '1')
                    ->setPageSize(10)
                    ->load();
        foreach ($collection as $product){
            $fea['id'] = (int)$product['entity_id'];
            $fea['name'] = $product['name'];
            $fea['sku'] = $product['sku'];
            //$fea['price'] = number_format($product['price'], 3);
            $fea['price'] = floatval($product['price']);


			$spericals = floatval($product['special_price']);

                if($spericals!=""){
                	$spericalps=$spericals;
                }else{
                	$spericalps="0.00";
                }

            $fea['special_price']=(int)$spericalps;

            $fea['file'] = $geturl.'pub/media/catalog/product'.$product['image'];

            $json['feature-list'][]=$fea;
        }

        /********* get feature product **************/

        //print_r($collection->getData());


        if($returnVal=="exist"){
            $data = ['list'=>$json, 'msg' => 'Data found.', 'error' => '1'];
        }else{
            $data = ['msg' => 'No data found.', 'error' => '0'];
        }

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);
        return $resultJson;

    }
}