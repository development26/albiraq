<?php
namespace Magecomp\Mobilelogin\Helper;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\Exception\LocalizedException;

class Apicall extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $scopeConfig;
    protected $_storeManager;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;

    }

    public function isEnable()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/moduleoption/enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getAuthkey()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/authkey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getRouttype()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/routtype',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getUsername()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/username',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getPassword()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/password',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getApiUrl()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/apiurl',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getSenderId()
    {
        return $this->scopeConfig->getValue(
            'mobilelogin/general/senderid',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }



    public function curlApiCall($message,$mobilenumbers)
    {

        try
        {
            $url=$this->getApiUrl();//"http://smsbox.com/smsgateway/services/messaging.asmx/Http_SendSMS?";
            $user = $this->getUsername();//$this->getApiUsername();
            $password = $this->getPassword();//$this->getApiPassword();

            $senderids=$this->getAuthkey();

            $customerids=$this->getSenderid();

            $ch = curl_init();
            if (!$ch){
                return "Couldn't initialize a cURL handle";
            }
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt ($ch, CURLOPT_POSTFIELDS,
                "username=$user&password=$password&customerid=$customerids&sendertext=$senderids&messagebody=$message&recipientnumbers=$mobilenumbers&defdate=&isblink=false&isflash=false");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $curlresponse = curl_exec($ch); // execute
            $curl_info = curl_getinfo($ch);

            $xml=simplexml_load_string($curlresponse);

            $array = json_decode(json_encode($xml), TRUE);

            //echo "<pre>"; print_r($array['Result']);

            if ($curlresponse == FALSE)
            {
                return "cURL error: ".curl_error($ch);
            }
            elseif($curl_info['http_code'] != 200)
            {
                return "Error: non-200 HTTP status code: ".$curl_info['http_code'];
            }
            else
            {
                $api_result = explode( '|', $curlresponse);
                $status_code = $api_result[0];
                if($status_code == '0' || $status_code == '1')
                {
                    return true;
                }
                //return "Error sending: status code [".$api_result[0]."] description [".$api_result[1]."]";
            }
            return $array['Result'];
            //curl_close($ch);
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }



    //         $url="http://smsbox.com/smsgateway/services/messaging.asmx/Http_SendSMS?username=$user&password=$password&customerid=515&sendertext=ROYAL PH&messagebody=$message&recipientnumbers=$mobilenumbers&defdate=&isblink=false&isflash=false";



    //         return "true";
    //     }
    //     else
    //     {
    //         return "false";
    //     }
    // }

}