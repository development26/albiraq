<?php
namespace Jsp\Placeholder\Plugin\Checkout\Block\Checkout\AttributeMerger;
class Plugin {
  public function afterMerge(\Magento\Checkout\Block\Checkout\AttributeMerger $subject, $result)
  {
    if (array_key_exists('street', $result)) {
      $result['street']['children'][0]['placeholder'] = __('Block');
      $result['street']['children'][1]['placeholder'] = __('Street');
      $result['street']['children'][2]['placeholder'] = __('Address 2: Line 1 *');
    }
      $result['city']['placeholder'] = __('City/Area *');
      $result['telephone']['placeholder']=__('Example: 96512345678');
      $result['firstname']['placeholder']=__('Enter first name');
      $result['lastname']['placeholder']=__('Enter last name');
      $result['postcode']['placeholder']=__('Zip/Postal Code');
      $result['username']['placeholder']=__('Enter email Address');
      $result['region']['placeholder']=__('State/Provice');
      $result['country_id']['placeholder']=__('Select Country');

    return $result;
  }
}