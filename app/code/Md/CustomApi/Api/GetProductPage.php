<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Md\CustomApi\Api;
interface GetProductPage {
    /**
     * @api
     * @param string $sku
     * @return array
     */
    public function getProductImageUrl($sku);
}