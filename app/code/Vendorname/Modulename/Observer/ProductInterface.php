<?php
/**
 * Copyright © 2018-2019 Hopescode, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Vendorname\Modulename\Observer;

use Magento\Framework\Event\ObserverInterface;
    use Magento\Catalog\Api\ProductRepositoryInterfaceFactory as ProductRepository;
    use Magento\Catalog\Helper\ImageFactory as ProductImageHelper;
    use Magento\Store\Model\StoreManagerInterface as StoreManager;
    use Magento\Store\Model\App\Emulation as AppEmulation;
    use Magento\Quote\Api\Data\CartItemExtensionFactory;

    class ProductInterface implements ObserverInterface
    {   
        /**
         * @var ObjectManagerInterface
         */
        protected $_objectManager;

        /**
         * @var ProductRepository
         */
        protected $productRepository;

        /**
         *@var \Magento\Catalog\Helper\ImageFactory
         */
        protected $productImageHelper;

        /**
         *@var \Magento\Store\Model\StoreManagerInterface
         */
        protected $storeManager;

        /**
         *@var \Magento\Store\Model\App\Emulation
         */
        protected $appEmulation;

        /**
         * @var CartItemExtensionFactory
         */
        protected $extensionFactory;

        /**
         * @param \Magento\Framework\ObjectManagerInterface $objectManager
         * @param ProductRepository $productRepository
         * @param \Magento\Catalog\Helper\ImageFactory
         * @param \Magento\Store\Model\StoreManagerInterface
         * @param \Magento\Store\Model\App\Emulation
         * @param CartItemExtensionFactory $extensionFactory
         */
        public function __construct(
            \Magento\Framework\ObjectManagerInterface $objectManager,
            ProductRepository $productRepository,
            ProductImageHelper $productImageHelper,
            StoreManager $storeManager,
            AppEmulation $appEmulation,
            CartItemExtensionFactory $extensionFactory
        ) {
            $this->_objectManager = $objectManager;
            $this->productRepository = $productRepository;
            $this->productImageHelper = $productImageHelper;
            $this->storeManager = $storeManager;
            $this->appEmulation = $appEmulation;
            $this->extensionFactory = $extensionFactory;
        }

    public function execute(\Magento\Framework\Event\Observer $observer, string $imageType = NULL)
        {
            $quote = $observer->getQuote();

           /**
             * Code to add the items attribute to extension_attributes
             */
            foreach ($quote->getAllItems() as $quoteItem) {
                $product = $this->productRepository->create()->getById($quoteItem->getProductId());
                $itemExtAttr = $quoteItem->getExtensionAttributes();
                if ($itemExtAttr === null) {
                    $itemExtAttr = $this->extensionFactory->create();
                }


                //$imageurl =$this->productImageHelper->create()->init($product, 'product_thumbnail_image')->setImageFile($product->getThumbnail())->getUrl();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface')->getById($quoteItem->getProductId());
                $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
                 
                $imageurl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
                
                if($product->getImage()==""){

                    $imageurlsimg=$store->getBaseUrl()."pub/media/catalog/product/placeholder/default/inputdataemtpy_2.jpg";

                }else{
                    $imageurlsimg=$imageurl;
                }

                $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($quoteItem->getProductId());
                $is_in_stocks=$productStockObj->getIsInStock();

                if($is_in_stocks==1){
                    $is_in_stock=true;
                }else{
                    $is_in_stock=false;
                }

                $config=$quoteItem->getProductType();

                if($config=="configurable"){
                    $sku=$quoteItem->getSku();
                    $namecong=$quoteItem->getName();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
                    // get product by product sku 
                    $productcon = $productRepository->get($sku);
                    $proname=$productcon->getName();
                }

                $arabicproducts = $objectManager->create('Magento\Catalog\Model\Product')->setStoreId(2)->load($quoteItem->getProductId());
                $arname = $arabicproducts->getName();


                $itemExtAttr->setImageUrl($imageurlsimg);
                $itemExtAttr->setIsInStock($is_in_stock);
                $itemExtAttr->setArname($arname);
                $quoteItem->setExtensionAttributes($itemExtAttr);
            }
            return;
        }

        /**
         * Helper function that provides full cache image url
         * @param \Magento\Catalog\Model\Product
         * @return string
         */
        protected function getImageUrl($product, string $imageType = NULL)
        {
            $storeId = $this->storeManager->getStore()->getId();

            $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
            $imageUrl = $this->productImageHelper->create()->init($product, $imageType)->getUrl();

            $this->appEmulation->stopEnvironmentEmulation();

            return $imageUrl;
        }

    }