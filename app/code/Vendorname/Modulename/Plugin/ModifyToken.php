<?php

namespace VendorName\ModuleName\Plugin;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Integration\Model\Oauth\Token\RequestThrottler;
use Magento\Integration\Model\CredentialsValidator;

class ModifyToken
{


/**
 * Customer Account Service
 *
 * @var AccountManagementInterface
 */
private $accountManagement;

/**
 * @var \Magento\Integration\Model\CredentialsValidator
 */
private $validatorHelper;

/**
 * @var RequestThrottler
 */
private $requestThrottler;

/**
 * Initialize service
 *
 * @param AccountManagementInterface $accountManagement
 * @param \Magento\Integration\Model\CredentialsValidator $validatorHelper
 */
public function __construct(
    AccountManagementInterface $accountManagement,
    CredentialsValidator $validatorHelper
) {
    $this->accountManagement = $accountManagement;
    $this->validatorHelper = $validatorHelper;
}

public function aroundCreateCustomerAccessToken(\Magento\Integration\Model\CustomerTokenService $subject, callable $proceed,$username, $password)
{
    $this->validatorHelper->validate($username, $password);
    $this->getRequestThrottler()->throttle($username, RequestThrottler::USER_TYPE_CUSTOMER);
    try {
        $customerDataObject = $this->accountManagement->authenticate($username, $password);
    } catch (\Exception $e) {
        $this->getRequestThrottler()->logAuthenticationFailure($username, RequestThrottler::USER_TYPE_CUSTOMER);
        $modifiedresult=array();   
        $modifiedresult['token']=null;
        $modifiedresult['error']="0";
        $modifiedresult['success']="false";
        $modifiedresult['msg']="Please check credentials";
        echo json_encode($modifiedresult);
        exit;
    }
    $result = $proceed($username, $password);
    return $result;
 }  

public function afterCreateCustomerAccessToken(\Magento\Integration\Model\CustomerTokenService $subject, $result)
{
$modifiedresult=array();
if($result){
  $modifiedresult['token']=$result;
  $modifiedresult['error']="1";
  $modifiedresult['success']="true";
  $modifiedresult['msg']="Token has been successfully generated";
}
echo json_encode($modifiedresult);
exit;
}

/**
 * Get request throttler instance
 *
 * @return RequestThrottler
 * @deprecated 100.0.4
 */
private function getRequestThrottler()
{
    if (!$this->requestThrottler instanceof RequestThrottler) {
        return \Magento\Framework\App\ObjectManager::getInstance()->get(RequestThrottler::class);
    }
    return $this->requestThrottler;
}

}