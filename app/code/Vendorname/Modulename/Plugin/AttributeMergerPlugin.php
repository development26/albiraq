<?php
namespace Vendorname\Modulename\Plugin;

class AttributeMergerPlugin
{
    public function afterMerge(\Magento\Checkout\Block\Checkout\AttributeMerger $subject, $result)
    {
        // add Additional class for country
        if (array_key_exists('country_id', $result)) {
              $result['country_id']['additionalClasses'] = 'custom-class-country';
        }
        // add Additional class for city
        // if (array_key_exists('city', $result)) {
        //     $result['city']['additionalClasses'] = 'cusotom-city';
        // }

        return $result;
    }
}