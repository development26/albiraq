<?php
namespace VendorName\ModuleName\Block;

class LayoutProcessor
{
     /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
     public function afterProcess(
         \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
         array  $jsLayout
     ) {
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
    ['shippingAddress']['children']['shipping-address-fieldset']['children']['street'] = [
        'component' => 'Magento_Ui/js/form/components/group',
        'label' => __('Address'),
        'required' => true,
        'dataScope' => 'shippingAddress.street',
        'provider' => 'checkoutProvider',
        'sortOrder' => 60,
        'type' => 'group',
        'children' => [
            [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input'
                ],
                'dataScope' => '0',
                'provider' => 'checkoutProvider',
                'placeholder' => 'Block',
                'validation' => ['required-entry' => true],
                'additionalClasses' => 'additional-block',
                'label' => __('Block')
            ],
            [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input'
                ],
                'dataScope' => '1',
                'provider' => 'checkoutProvider',
                'placeholder' => 'Street',
                'validation' => ['required-entry' => true],
                'additionalClasses' => 'additional-street',
                'label' => __('street')
            ],
            [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input'
                ],
                'dataScope' => '2',
                'provider' => 'checkoutProvider',
                'placeholder' => 'Address',
                'validation' => ['required-entry' => false],
                'additionalClasses' => 'additional-house',
                'label' => __('House no/floor/flat')
                ]
            ]
            
        ];

         return $jsLayout;
     }
 }