<?php

namespace MageWorx\SearchSuiteAutocomplete\Model\Source;

class ProductFields
{
    const NAME = 'name';

    const SKU = 'sku';

    const IMAGE = 'image';

    const REVIEWS_RATING = 'reviews_rating';

    const SHORT_DESCRIPTION = 'short_description';

    const DESCRIPTION = 'description';

    const PRICE = 'price';

    const ADD_TO_CART = 'add_to_cart';

    const URL = 'url';

    const SPECIALPRICE = 'special_price';

    const ID = 'entity_id';
    const STOCKSTATUS = 'is_in_stock';
    const SPECIALFROMDATE = 'special_form_date';
    const SPECIALTODATE = 'special_to_date';

    const ARNAME = 'arname';

    const HIDEADDTOCART = 'hide_add_to_cart';

    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $this->options = [
            ['value' => self::NAME, 'label' => __('Product Name')],
            ['value' => self::SKU, 'label' => __('SKU')],
            ['value' => self::IMAGE, 'label' => __('Product Image')],
            ['value' => self::REVIEWS_RATING, 'label' => __('Reviews Rating')],
            ['value' => self::SHORT_DESCRIPTION, 'label' => __('Short Description')],
            ['value' => self::DESCRIPTION, 'label' => __('Description')],
            ['value' => self::PRICE, 'label' => __('Price')],
            ['value' => self::ADD_TO_CART, 'label' => __('Add to Cart Button')],
            ['value' => self::SPECIALPRICE, 'label' => __('Special Price')],
            ['value' => self::ID, 'label' => __('Product Id')],
            ['value' => self::STOCKSTATUS, 'label' => __('Stock Status')],
            ['value' => self::SPECIALFROMDATE, 'label' => __('Special Form Date')],
            ['value' => self::SPECIALTODATE, 'label' => __('Special To Date')],
            ['value' => self::ARNAME, 'label' => __('Arname')],
            ['value' => self::HIDEADDTOCART, 'label' => __('Hide Add To Cart')],
        ];

        return $this->options;
    }
}
