<?php
namespace MageBig\WidgetPlus\Model\ResourceModel\Widget\Collection;

/**
 * Interceptor class for @see \MageBig\WidgetPlus\Model\ResourceModel\Widget\Collection
 */
class Interceptor extends \MageBig\WidgetPlus\Model\ResourceModel\Widget\Collection implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Data\Collection\EntityFactory $entityFactory, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility, \Magento\Catalog\Model\Config $catalogConfig, \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory, \Magento\Framework\App\ResourceConnection $resource, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Registry $registry, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Catalog\Model\CategoryFactory $categoryFactory, \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate, \Magento\Customer\Model\Session $customerSession, \MageBig\WidgetPlus\Model\Rule $rule)
    {
        $this->___init();
        parent::__construct($entityFactory, $productCollectionFactory, $catalogProductVisibility, $catalogConfig, $productsFactory, $resource, $storeManager, $registry, $checkoutSession, $categoryFactory, $localeDate, $customerSession, $rule);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurPage($displacement = 0)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurPage');
        return $pluginInfo ? $this->___callPlugins('getCurPage', func_get_args(), $pluginInfo) : parent::getCurPage($displacement);
    }
}
