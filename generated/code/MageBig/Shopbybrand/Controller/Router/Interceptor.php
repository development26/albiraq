<?php
namespace MageBig\Shopbybrand\Controller\Router;

/**
 * Interceptor class for @see \MageBig\Shopbybrand\Controller\Router
 */
class Interceptor extends \MageBig\Shopbybrand\Controller\Router implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\ActionFactory $actionFactory, \MageBig\Shopbybrand\Model\BrandFactory $brandFactory, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \MageBig\Shopbybrand\Helper\Data $brandHelper)
    {
        $this->___init();
        parent::__construct($actionFactory, $brandFactory, $storeManager, $scopeConfig, $brandHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'match');
        return $pluginInfo ? $this->___callPlugins('match', func_get_args(), $pluginInfo) : parent::match($request);
    }
}
