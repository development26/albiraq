<?php
namespace Magecomp\Codverification\Controller\Sendotp\Otp;

/**
 * Interceptor class for @see \Magecomp\Codverification\Controller\Sendotp\Otp
 */
class Interceptor extends \Magecomp\Codverification\Controller\Sendotp\Otp implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magecomp\Codverification\Helper\Apicall $helperapi, \Magecomp\Codverification\Helper\Data $helperdata, \Magento\Framework\Controller\Result\JsonFactory $resultJson, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Email\Model\Template\Filter $filter)
    {
        $this->___init();
        parent::__construct($context, $helperapi, $helperdata, $resultJson, $checkoutSession, $filter);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
