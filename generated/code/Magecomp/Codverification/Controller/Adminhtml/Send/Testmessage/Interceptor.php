<?php
namespace Magecomp\Codverification\Controller\Adminhtml\Send\Testmessage;

/**
 * Interceptor class for @see \Magecomp\Codverification\Controller\Adminhtml\Send\Testmessage
 */
class Interceptor extends \Magecomp\Codverification\Controller\Adminhtml\Send\Testmessage implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magecomp\Codverification\Helper\Apicall $helperapi, \Magecomp\Codverification\Helper\Data $helperdata)
    {
        $this->___init();
        parent::__construct($context, $helperapi, $helperdata);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
