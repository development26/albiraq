<?php
namespace Magecomp\Codverification\Controller\Verify\Otp;

/**
 * Interceptor class for @see \Magecomp\Codverification\Controller\Verify\Otp
 */
class Interceptor extends \Magecomp\Codverification\Controller\Verify\Otp implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magecomp\Codverification\Helper\Data $helperdata, \Magento\Framework\Controller\Result\JsonFactory $resultJson, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Framework\Json\Helper\Data $jsonhelper)
    {
        $this->___init();
        parent::__construct($context, $helperdata, $resultJson, $checkoutSession, $jsonhelper);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
