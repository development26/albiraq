<?php
namespace Magecomp\Mobilelogin\Controller\Index\Updatemobile;

/**
 * Interceptor class for @see \Magecomp\Mobilelogin\Controller\Index\Updatemobile
 */
class Interceptor extends \Magecomp\Mobilelogin\Controller\Index\Updatemobile implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magecomp\Mobilelogin\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
