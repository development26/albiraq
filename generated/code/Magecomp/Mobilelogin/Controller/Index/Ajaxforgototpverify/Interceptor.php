<?php
namespace Magecomp\Mobilelogin\Controller\Index\Ajaxforgototpverify;

/**
 * Interceptor class for @see \Magecomp\Mobilelogin\Controller\Index\Ajaxforgototpverify
 */
class Interceptor extends \Magecomp\Mobilelogin\Controller\Index\Ajaxforgototpverify implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magecomp\Mobilelogin\Helper\Data $helperData)
    {
        $this->___init();
        parent::__construct($context, $helperData);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
