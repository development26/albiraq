<?php
namespace Magecomp\Mobilelogin\Controller\Index\Updatemobilenumber;

/**
 * Interceptor class for @see \Magecomp\Mobilelogin\Controller\Index\Updatemobilenumber
 */
class Interceptor extends \Magecomp\Mobilelogin\Controller\Index\Updatemobilenumber implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Customer\Model\Customer $customer, \Magento\Customer\Model\Data\Customer $customerData, \Magento\Customer\Model\ResourceModel\Customer $customerResource, \Magento\Customer\Model\ResourceModel\CustomerFactory $customerResourceFactory, $data = [])
    {
        $this->___init();
        parent::__construct($context, $customerFactory, $customer, $customerData, $customerResource, $customerResourceFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
