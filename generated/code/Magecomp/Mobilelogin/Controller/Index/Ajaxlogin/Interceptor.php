<?php
namespace Magecomp\Mobilelogin\Controller\Index\Ajaxlogin;

/**
 * Interceptor class for @see \Magecomp\Mobilelogin\Controller\Index\Ajaxlogin
 */
class Interceptor extends \Magecomp\Mobilelogin\Controller\Index\Ajaxlogin implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Magento\Customer\Model\Account\Redirect $accountRedirect, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magecomp\Mobilelogin\Helper\Data $helperData)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $jsonResultFactory, $customerSession, $formKeyValidator, $customerAccountManagement, $cookieMetadataFactory, $accountRedirect, $storeManager, $helperData);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
