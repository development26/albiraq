<?php
namespace Magecomp\Cityandregionmanager\Controller\Adminhtml\Importcitieslist\Import;

/**
 * Interceptor class for @see \Magecomp\Cityandregionmanager\Controller\Adminhtml\Importcitieslist\Import
 */
class Interceptor extends \Magecomp\Cityandregionmanager\Controller\Adminhtml\Importcitieslist\Import implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\File\Csv $csv, \Magecomp\Cityandregionmanager\Model\Cities $citiesListModel, \Magento\Framework\App\Filesystem\DirectoryList $directoryList, \Magecomp\Cityandregionmanager\Model\ResourceModel\Cities\CollectionFactory $citiesListCollectionFactory)
    {
        $this->___init();
        parent::__construct($context, $csv, $citiesListModel, $directoryList, $citiesListCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
