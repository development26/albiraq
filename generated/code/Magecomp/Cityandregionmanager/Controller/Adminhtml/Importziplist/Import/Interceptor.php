<?php
namespace Magecomp\Cityandregionmanager\Controller\Adminhtml\Importziplist\Import;

/**
 * Interceptor class for @see \Magecomp\Cityandregionmanager\Controller\Adminhtml\Importziplist\Import
 */
class Interceptor extends \Magecomp\Cityandregionmanager\Controller\Adminhtml\Importziplist\Import implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\File\Csv $csv, \Magecomp\Cityandregionmanager\Model\Zip $zipListModel, \Magento\Framework\App\Filesystem\DirectoryList $directoryList, \Magecomp\Cityandregionmanager\Model\ResourceModel\Zip\CollectionFactory $zipListCollectionFactory)
    {
        $this->___init();
        parent::__construct($context, $csv, $zipListModel, $directoryList, $zipListCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
