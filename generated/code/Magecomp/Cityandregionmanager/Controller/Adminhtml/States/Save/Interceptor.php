<?php
namespace Magecomp\Cityandregionmanager\Controller\Adminhtml\States\Save;

/**
 * Interceptor class for @see \Magecomp\Cityandregionmanager\Controller\Adminhtml\States\Save
 */
class Interceptor extends \Magecomp\Cityandregionmanager\Controller\Adminhtml\States\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Backend\Model\Session $session, \Magecomp\Cityandregionmanager\Model\States $model)
    {
        $this->___init();
        parent::__construct($context, $session, $model);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
