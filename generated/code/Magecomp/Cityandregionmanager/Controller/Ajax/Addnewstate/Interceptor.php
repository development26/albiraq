<?php
namespace Magecomp\Cityandregionmanager\Controller\Ajax\Addnewstate;

/**
 * Interceptor class for @see \Magecomp\Cityandregionmanager\Controller\Ajax\Addnewstate
 */
class Interceptor extends \Magecomp\Cityandregionmanager\Controller\Ajax\Addnewstate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magecomp\Cityandregionmanager\Model\States $statesModel, \Magecomp\Cityandregionmanager\Model\Config $config, \Magento\Catalog\Model\Product $product, \Magento\Framework\View\Result\Page $page, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $statesModel, $config, $product, $page, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
