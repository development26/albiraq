<?php
namespace Magecomp\Cityandregionmanager\Controller\Ajax\Getcities;

/**
 * Interceptor class for @see \Magecomp\Cityandregionmanager\Controller\Ajax\Getcities
 */
class Interceptor extends \Magecomp\Cityandregionmanager\Controller\Ajax\Getcities implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magecomp\Cityandregionmanager\Model\ResourceModel\Cities\CollectionFactory $citiesCollection, \Magecomp\Cityandregionmanager\Model\ResourceModel\GroupCities\CollectionFactory $GroupCitiesCollection, \Magecomp\Cityandregionmanager\Model\Config $config, \Magento\Catalog\Model\Product $product, \Magento\Framework\View\Result\Page $page, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $citiesCollection, $GroupCitiesCollection, $config, $product, $page, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
