<?php
namespace Meetanshi\Cbk\Controller\Payment\Redirect;

/**
 * Interceptor class for @see \Meetanshi\Cbk\Controller\Payment\Redirect
 */
class Interceptor extends \Meetanshi\Cbk\Controller\Payment\Redirect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Registry $registry, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Sales\Model\OrderFactory $orderFactory, \Magento\Framework\App\Request\Http $request, \Magento\Sales\Model\Order\Payment\Transaction\Builder $transactionBuilder, \Meetanshi\Cbk\Helper\Data $helper, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Sales\Model\OrderNotifier $orderSender, \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender, \Magento\Framework\DB\TransactionFactory $transactionFactory, \Magento\Sales\Model\Service\InvoiceService $invoiceService, \Magento\Framework\Controller\Result\JsonFactory $jsonFactory)
    {
        $this->___init();
        parent::__construct($context, $registry, $checkoutSession, $storeManager, $scopeConfig, $orderFactory, $request, $transactionBuilder, $helper, $resultPageFactory, $orderSender, $invoiceSender, $transactionFactory, $invoiceService, $jsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
