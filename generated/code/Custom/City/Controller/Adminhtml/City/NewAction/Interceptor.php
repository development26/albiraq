<?php
namespace Custom\City\Controller\Adminhtml\City\NewAction;

/**
 * Interceptor class for @see \Custom\City\Controller\Adminhtml\City\NewAction
 */
class Interceptor extends \Custom\City\Controller\Adminhtml\City\NewAction implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Custom\City\Model\CityFactory $cityFactory, \Magento\Framework\App\Filesystem\DirectoryList $directoryList, \Magento\Framework\File\Csv $csv, \Custom\City\Model\Resource\State\Collection $stateCollection, \Custom\City\Model\Resource\City\Collection $cityCollection)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $cityFactory, $directoryList, $csv, $stateCollection, $cityCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
