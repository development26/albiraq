<?php
namespace Custom\City\Controller\Adminhtml\State\Grid;

/**
 * Interceptor class for @see \Custom\City\Controller\Adminhtml\State\Grid
 */
class Interceptor extends \Custom\City\Controller\Adminhtml\State\Grid implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Custom\City\Model\StateFactory $stateFactory, \Magento\Framework\App\Filesystem\DirectoryList $directoryList, \Magento\Framework\File\Csv $csv, \Custom\City\Model\Resource\State\Collection $stateCollection, \Custom\City\Model\Resource\Statelocale\Collection $stateLocaleCollection, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Custom\City\Model\Statelocale $stateLocale, \Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $stateFactory, $directoryList, $csv, $stateCollection, $stateLocaleCollection, $storeManager, $scopeConfig, $stateLocale, $resourceConnection);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
