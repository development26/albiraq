<?php
namespace Custom\City\Controller\Adminhtml\Zip\Delete;

/**
 * Interceptor class for @see \Custom\City\Controller\Adminhtml\Zip\Delete
 */
class Interceptor extends \Custom\City\Controller\Adminhtml\Zip\Delete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Custom\City\Model\ZipFactory $zipFactory, \Magento\Framework\App\Filesystem\DirectoryList $directoryList, \Magento\Framework\File\Csv $csv, \Custom\City\Model\Resource\City\Collection $cityCollection, \Custom\City\Model\Resource\Zip\Collection $zipCollection, \Custom\City\Model\Resource\State\Collection $stateCollection)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $zipFactory, $directoryList, $csv, $cityCollection, $zipCollection, $stateCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }
}
