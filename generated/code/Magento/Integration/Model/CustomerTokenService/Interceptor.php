<?php
namespace Magento\Integration\Model\CustomerTokenService;

/**
 * Interceptor class for @see \Magento\Integration\Model\CustomerTokenService
 */
class Interceptor extends \Magento\Integration\Model\CustomerTokenService implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Integration\Model\Oauth\TokenFactory $tokenModelFactory, \Magento\Customer\Api\AccountManagementInterface $accountManagement, \Magento\Integration\Model\ResourceModel\Oauth\Token\CollectionFactory $tokenModelCollectionFactory, \Magento\Integration\Model\CredentialsValidator $validatorHelper, ?\Magento\Framework\Event\ManagerInterface $eventManager = null)
    {
        $this->___init();
        parent::__construct($tokenModelFactory, $accountManagement, $tokenModelCollectionFactory, $validatorHelper, $eventManager);
    }

    /**
     * {@inheritdoc}
     */
    public function createCustomerAccessToken($username, $password)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'createCustomerAccessToken');
        return $pluginInfo ? $this->___callPlugins('createCustomerAccessToken', func_get_args(), $pluginInfo) : parent::createCustomerAccessToken($username, $password);
    }
}
