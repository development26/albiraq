<?php
namespace Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver;

/**
 * Interceptor class for @see \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver
 */
class Interceptor extends \Magento\Elasticsearch\Model\Adapter\FieldMapper\FieldMapperResolver implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, array $fieldMappers = [])
    {
        $this->___init();
        parent::__construct($objectManager, $fieldMappers);
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldName($attributeCode, $context = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFieldName');
        return $pluginInfo ? $this->___callPlugins('getFieldName', func_get_args(), $pluginInfo) : parent::getFieldName($attributeCode, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllAttributesTypes($context = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAllAttributesTypes');
        return $pluginInfo ? $this->___callPlugins('getAllAttributesTypes', func_get_args(), $pluginInfo) : parent::getAllAttributesTypes($context);
    }
}
