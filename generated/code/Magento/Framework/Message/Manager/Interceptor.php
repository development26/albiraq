<?php
namespace Magento\Framework\Message\Manager;

/**
 * Interceptor class for @see \Magento\Framework\Message\Manager
 */
class Interceptor extends \Magento\Framework\Message\Manager implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Message\Session $session, \Magento\Framework\Message\Factory $messageFactory, \Magento\Framework\Message\CollectionFactory $messagesFactory, \Magento\Framework\Event\ManagerInterface $eventManager, \Psr\Log\LoggerInterface $logger, $defaultGroup = 'default', ?\Magento\Framework\Message\ExceptionMessageFactoryInterface $exceptionMessageFactory = null)
    {
        $this->___init();
        parent::__construct($session, $messageFactory, $messagesFactory, $eventManager, $logger, $defaultGroup, $exceptionMessageFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function addComplexSuccessMessage($identifier, array $data = [], $group = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'addComplexSuccessMessage');
        return $pluginInfo ? $this->___callPlugins('addComplexSuccessMessage', func_get_args(), $pluginInfo) : parent::addComplexSuccessMessage($identifier, $data, $group);
    }
}
